# Hiking in Bansko

What do you need while hiking in the mountains?

 - Comfortable waterproof hiking shoes or/and use waterproof spray.
 - Comfortable clothing, spare warm clothing (incl. hat and gloves), waterproof jacket.
 - Sun hat, sunglasses, sun cream SPF > 30.
 - Refillable water container ~ 1 l.
 - Paper map of the mountain. Offline map on your smartphone: the app maps.me for example – you need to download the map of the region in advance.
 - Personal first aid kit and any prescribed medication.
 - Food (sandwiches, nuts, dried fruits, snacks).
 - Rucksack.
 - A pair of telescopic poles makes the walking easier.
 - Mountain insurance.
 - Charged phone.

## What to be aware of?

 - Weather changes quickly in the mountain and temperature is lower than in the nearby towns.
 - In June, there are still big snow spots on the way. Cross them as fast as possible.
 - As you may have to cross a river or a spring, note that the water level in the rivers and the springs is higher in June until mid-July, as well as after a heavy rain. Later in the summer some of them may completely dry.
 - If possible, do not go on your own and inform someone else where and when you are going or hire a qualified mountain leader.


## Useful Telephone Numbers

 - Mountain Rescue Service: +359 88 1474; +359 749 88 132
 - Vihren hut: +359 889834277
 - Banderitsa hut: +359 898868999, +359 894340343
 - Demyanitsa hut: +359 878688135, +359 877688135
 - Yavorov hut: 0896/688415, 0896/688413
 - Bezbog hut: +359 888286102, +359 885502801, +359 74472120
 - Gotze Delchev hut: +359 7476209
 - Zagaza hut: +359 879601133, +359 878966204, +359 894212434,
    +359 74333108
 - Sinanitsa hut: +359 896798040
 - Tevno ezero shelter: +359 886397268, +359 884 663635
 - Spano pole shelter: +359 896688407, +359 896688408
 - Kamenitza (Begovitsa) hut: +359 889811000, +359 7463198
 - Pirin hut: +359 896766152