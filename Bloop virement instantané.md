# Bloop virement instantané

Besoin de tester avec nouvelle branche transfer service, mais virement blopé actuellement:

Transfer-service:
 - https://review-rst-842-bl-t4d0a5-transfer-transac.hors-prod.caas.lcl.gca

Update common-jwt too:
 - https://scm.sws.group.gca/lcl_fr/commons/common-jwt/tree/RST-842/bloop_instant_transfer



Log
 - Updated common-jwt with new operation code for instant transfer
 - Testing transfer branch that checks this operation code
 - Where does this operation code get assigned?
 - Transfer-Service branch https://scm.saas.cagip.group.gca/lcl/bpi/transac/belpp-03631-metier/transfer-service/-/tree/RST-842/bloop_instant_transfer

 - [x] Create an endpoint that verifies permission
	 - [x] Add Tests
 - [x] Call endpoint successfully from BFF
 - [x] Prevent tick off 
	 - [x] Prevent tick off with result from http request to transfer-service

 - [x] Call modal
	 - Inspiration: [Sarah code on error handling](https://scm.saas.cagip.group.gca/lcl/bpi/transac/belpp-02262-ui/transac-frontend/commit/32c69c811b78c14264bc66600777a4a90a71f3a8)
 - [ ] Process outstanding feedback on existing PR 
 - [ ] Debug why not working
	 - Could I have implemented the wrong thing on the back?


## Broken Tests

      it('should assign transfer limit to provisional_balance_authorized_overdraft if internal transfer', () => {
      it('should assign transfer limit to ceiling_amount if external transfer and ceiling_amount smaller than provisional_balance_authorized_overdraft', () => {
      it('should assign transfer limit to provisional_balance_authorized_overdraft if external transfer and provisional_balanced_authorized_overdraft smaller than ceiling_amount', () => {
    it('should set isInstantPayment to true when checkbox is checked', () => {
    it('should send a tag when clicking validation button and instantPayment is checked', () => {
