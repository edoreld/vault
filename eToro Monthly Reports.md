# eToro Monthly Reports

## July 2020

Dear copiers,

Another month has passed. Shall we take a look at how the portfolio did?

For July, we are up by 8.86%, which means that we are yet again beating the $SPX500 (+3.68%) by a considerable amount. Our portfolio is now 28.44% up for the year.

A few exciting companies found their way into their portfolio. Among these $Z, $CRWD, $TTD and my personal favorite, $PS. You can read more about Pluralsight on my pinned post at https://etoro.tw/3fcBFm7.

That's all for the month, take care!

@edoreld






