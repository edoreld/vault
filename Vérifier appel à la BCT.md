# Vérifier appel à la BCT

Tracked in: [Vérifier appel à la BCT](https://jira-agile.fr.cly:8443/browse/RST-728)
Related to: [[Virement]]
Effects: [[Transfer-Service]]
Objective: make the call to BCT return accounts
Swagger (Swarm): in Internet Explorer [this url](http://dvl-doc-datalayer.lclhp-ee.docker.prodinfo.gca/#/alpha/get_comptes) 
Swagger (Kube): use Postman or ask the team

## Notes
 - Calling the BCT works with the Kube Dev url

## Debugging

Caused by: org.springframework.web.client.HttpServerErrorException$InternalServerError: 500 : [{"code":"B0026","message":"LE COMPTE DONNEUR D'ORDRE EST INVALIDE"}]

I suspect that it's caused by using a random IBAN from the internets. 

## Tests

The only one that should work atm is Kube dev. Swarm ones have the certificate problem.

Swarm :
```
 curl -k --request GET 'https://dvl-bct-datalayer.lclhp-ee.docker.prodinfo.gca/bct/contract/0039200007681974/ibans' --header 'accept: application/json' 
 curl -k --request GET 'https://rct-bct-datalayer.lclhp-ee.docker.prodinfo.gca/bct/contract/0039200007681974/ibans' --header 'accept: application/json' 
```

Kube :
```
 curl -k --request GET 'https://bct-service-cntbel-03823-metier-development.hors-prod.caas.lcl.gca/bct/contract/0039200007681974/ibans' --header 'accept: application/json'
 curl -k --request GET 'https://bct-service-cntbel-03823-metier-uat.hors-prod.caas.lcl.gca/bct/contract/0039200007681974/ibans' --header 'accept: application/json'

```

I've contacted BCT team on 16/06/2020 at 16:00 and they say they are restarting services.

## URLs

Swarm Dev (dvl = dev): https://dvl-bct-datalayer.lclhp-ee.docker.prodinfo.gca
Swarm Rct: https://rct-bct-datalayer.lclhp-ee.docker.prodinfo.gca
Kube Dev: https://bct-service-cntbel-03823-metier-development.hors-prod.caas.lcl.gca/
Kube Rct (uat = recette): https://bct-service-cntbel-03823-metier-uat.hors-prod.caas.lcl.gca 

Current state: mongodb issue en recette 


## Swagger
In Internet Explorer, open [this url](http://dvl-doc-datalayer.lclhp-ee.docker.prodinfo.gca/#/alpha/get_comptes) 


### Old Swagger
```
openapi: 3.0.0

info:
  title: Contrats BCT
  description: Service exposant la manipulation des contrats BCT.
  version: '${project.version}'
  contact:
    name: LCL Squad
    email: ldd_lcl_itp_bpi_datalayer@lcl.fr
  license:
    name: Tous droits réservés

servers:
  #- url: https://rct-agregation.devinfo.fr.cly
  #  description: RP de recette
  #- url: https://dvl-agregation.devinfo.fr.cly
  #  description: RP de dev
  #- url: https://dvl-banques-datalayer.lclhp-ee.docker.prodinfo.gca
  #  description: hors-prod-dvl
  #- url: https://rct-banques-datalayer.lclhp-ee.docker.prodinfo.gca
  #  description: hors-prod-rct
  - url: http://localhost:8097
    description: local

paths:
  /bct/contracts/{personId}:
    get:
      summary: Restituer la liste des contrats BCT liés à un identifiant personne donné
      description: |
        À partir de l'identifiant d'une personne (idReper), ce WS récupère la liste des contrats BCT.

        Cette liste contiendra les id des contrats BCT.

        La liste fournie est vide si la personne ne contient aucun contrat BCT.

      parameters:
        - in: path
          name: personId
          required: true
          description: Identifiant technique de la personne dont on récupère la liste des contrats.
          schema:
            type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/BctListeContrats'

  /bct/contract/{contractId}/idreper:
    get:
      summary: Restituer l'identifiant personne (idReper) lié à un contrat BCT donné
      description: |
        À partir de l'identifiant de contrat BCT, cette action récupère l'identifiant de la personne (idReper).

      parameters:
        - in: path
          name: contractId
          required: true
          description: Identifiant technique du contrat dont l'id de la personne associée est à récupérer.
          schema:
            type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BctIdentifiantPersonne'

  /bct/contract/{contractId}/accounts:
    get:
      summary:  Restituer la liste des comptes appartenant à un contrat BCT donné.
      description: |
        À partir de l'identifiant de contrat BCT, cette action récupère la liste des comptes associés.

        Cette liste contiendra les informations de comptes suivantes :
          * Le numéro d'agence LCL
          * Le numéro de compte
          * La lettre clé

        La liste fournie est vide si aucun compte n'est associé au contrat.

      parameters:
        - in: path
          name: contractId
          required: true
          description: Identifiant technique du contrat dont on récupère la liste des comptes.
          schema:
            type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/BctComptes'

  /bct/contract/{contractId}/ibans:
    get:
      summary:  Restituer la liste des IBANS appartenant à un contrat BCT donné.
      description: |
        À partir de l'identifiant de contrat BCT, cette action récupère la liste des IBANS associés.

        Cette liste contiendra les informations suivantes :
          * Iban
          * Intitulé de l'Iban

        La liste fournie est vide si aucun IBAN n'est associé au contrat.

      parameters:
        - in: path
          name: contractId
          required: true
          description: Identifiant technique du contrat dont on récupère la liste des IBANS.
          schema:
            type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/BctIban'

security:
  - bearerAuth: []

components:
  securitySchemes:
    bearerAuth:            # arbitrary name for the security scheme
      type: http
      scheme: bearer
      bearerFormat: JWT    # optional, arbitrary value for documentation purposes

  responses:
    JetonInvalide:
      description: |
        L'authentification de l'utilisateur est invérifiée.

        Cette erreur peut notamment être due :

        * à un jeton inutilisable pour le service demandé ;
        * à un jeton dont la durée de validité est dépassée.

    MauvaisRelais:
      description: |
        Mauvaise réponse reçue d'un serveur intermédiaire.

        Une ressource indispensable au bon traitement de la requête a fourni
        une réponse inattendue. En conséquence, le service demandé ne peut être fourni.

    DroitsInsuffisants:
      description: |
        Les droits accordés sont insuffisants pour accéder à cette ressource.

        L'accès à la ressource demandée requiert des droits supplémentaires à ceux
        accordés à cet utilisateur.

    RessourceIntrouvable:
      description: |
        La ressource demandée est introuvable.

        Dans le cas où la ressource en question a été fournie par l'un des services
        Datalayer, il se peut qu'elle ait été supprimée depuis.

    RequeteInvalide:
      description: |
        La requête fournie est incorrecte.

        Au moins l'un des éléments constituant la requête est invalide ou inutilisable.

  schemas:
    BctListeContrats:
      type: array
      description: Liste des id contrats associés à une personne donnée.
      example: '1234567891234567, 1234567891234567'

    BctIdentifiantPersonne:
      type: string
      description: Id de la personne liée au contrat BCT donné.
      example: '1234567891'

    BctComptes:
      type: object
      description: Liste des comptes.
      properties:
        agence_compte_et_cle:
          type: string
          description: numéro de l'agence et la clé.

          example: '00222368542X'
        nature_compte:
          type: string
          description: Nature du compte.
            Deux valeurs possibles -> DEPO / CSL

          example: 'DEPO'
        CACT:
          type: string
          description: La lettre clé.

          example: '001'

    BctIban:
      type: object
      description: Liste des Ibans.
      properties:
        iban:
          type: string
          description: Iban
          example: 'FR3730002003340000017009H95'
        intituleIBAN:
          type: string
          description: Intitulé de l'Iban
          example: 'irma1'
```