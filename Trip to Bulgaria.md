# Trip to Bulgaria
Day Planner: [[Bulgaria Day Planner]]

## Depart

Tuesday, July 15 - 10:20 -	DEPART: Paris, Charles De Gaulle (CDG)
Tuesday, July 15 - 14:05 -  ARRIVE: Sofia, Sofia (SOF) 

## Return

Sunday, July 26 - 20:20 - DEPART: Sofia, Sofia (SOF)
Sunday, July 26 - 22:20 - ARRIVAL: Paris, Charles de Gaulle (CDG)

Roughly, the plan is to stay in Sofia a few days and then go to Bansko



## IMPORTANT

 -> Money exchange
 
 You can use ATMs at Banks but don't use the standalone ATMs! They have horrible exchange rates.
 
 The local currency is lev, which is accepted everywhere.
 
 1 euro is worth roughly 2 lev. 
 

### Sofia

### Bansko
	
#### Market
	
The market is open everyday from 9 to 19h and the typical market day is Sunday! And there’s a Butchers, if you want to buy meat.

The Bansko Market [map](https://goo.gl/maps/13zinkmf67A2)
The local market [map](https://goo.gl/maps/s1J71b3tKKQ2)
	
	
	
	
	

