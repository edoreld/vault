# Angular

## Decorators

 - They are a Typescript feature
 - Anything that starts with a `@` is a decorator
 - Provide metadata 
## Template Reference Variabless (#)

\# Variable declaration
 - declares DOM elements as variables
 - \#var = Template reference variable

```js
<input #phone placeholder="phone number">
```

^ Creates a reference to the input element that can be used somewhere else in the template.

```js
<!-- phone refers to the input element --> 
<button (click)="callPhone(phone.value)">Call</button>
```

() - event binding

[] - property binding

[()] - two-way property binding

{{ }} - interpolation

* - structural directives

## Injection Tokens

## Double Exclamation Operation (!!)

Converts variables to Boolean