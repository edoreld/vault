# Where to eat in Bansko

##### Coffee Shops

Old Town actually boasts quite a few Coffee Shops for that perfect break no matter what part of the day that falls for you.

  - Cafe Elize
  - Cafe Retro
  - Le Retro
  - Cafe LIDO (for Coffee and Cake!)
  - Starfish
	
##### Breakfast
 - Riverside Hotel’s Buffet:  **decent buffet selection**. 
 - Le Retro Bansko: Breakfast service from 8am till 11am
 - Momini dvori: 
	• English breakfast for 13lv
	• Sandwiches from 7lv
	• Mekitsi for 6lv
 - Coconut
	 - Open: 10AM–1PM, 4–9PM
	 - A small space for smoothies and coffee .

##### Lunch
There are special lunch deals at several places around town starting at 2.5€

##### Dinner 
The Co-working Bansko team go out for dinner frequently and the members have started a list of some of their best picks in town (great price and quality). Expect about 5–10€ for dinner.

##### Some places to eat
  - Restaurant Kancheto [map](https://goo.gl/maps/qLutqAJPEA92)
    Restaurant Victoria [map](https://goo.gl/maps/A6JrQTNE91s)
    Hotel Papi (they serve decent Sushi here) [map](https://goo.gl/maps/SDEJZAqYb752)
    Restaurant Eagles Nest (Affordable local food) [map](https://goo.gl/maps/Bc6rVxepBDH2)
    Adela (Great local food — lunch menus for about 5 Lev) [map](https://goo.gl/maps/dS2bNxGLHPS2)
    Smoky Mountain (English Pub — Co-working Bansko members get a discount) [map](https://goo.gl/maps/t4DRKf4QUEs)
	