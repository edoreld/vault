# Personal Knowledge Management
#knowledge-management #productivity #creativity

In the course [[C! Building a second brain with Tiago Forge | Building a second brain with Tiago Forge]], Tiago defines [[Personal Knowledge Management]] as the combination of the technology and human aspect in order to turn information into knowledge.## Backlinks
* [[Personal Knowledge Management]]
	* In the course [[C! Building a second brain with Tiago Forge | Building a second brain with Tiago Forge]], Tiago defines [[Personal Knowledge Management]] as the combination of the technology and human aspect in order to turn information into knowledge.
* [[C! Building a second brain with Tiago Forge]]
	* He shows a video of a F1 pit stop. In the video we see a pit stop from the past and one from the present. The one from the past takes more than 1 minute, while the one from the present takes a few seconds. He talks about how the combination of technology and humans aspects have enabled this improvement, and he compares it to [[Personal Knowledge Management]].
	* He defines [[Personal Knowledge Management]] as the combination of the technology and human aspects in order to turn information into knowledge.

