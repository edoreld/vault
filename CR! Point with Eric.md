# CR! Point with Eric

## Notes
 - He has noted my juniority/inexperience as a possible cause for some of the behaviors that didn't match with what they expected as a team.
 - He has acknoledged that I have done improvements
 - He has said that the team can't afford to wait until I make enough improvements to match their expectations.

## Improvement Points
  - Communicate better (i.e: daily)
  - When blocked, be able to move to another task and push people (Eric, Alexandre) to unlock the blocking points (i.e: relances)
  - Favor the communication of the team. In this case, the principle is to aid people directly. Sharing link to documentation should be secondary or used when there is a lot of information to communicate. 


In general, adapt to how the team does things, rather than do things your own way