# Vérifier qu'on est toujours capables de livrer la mire

U.S: https://jira-agile.fr.cly:8443/browse/RST-1060

Afin de nous assurer de notre capacité à livrer la mire,  nous souhaiterions essayer de la redéployer sur pilote (notre environnement le plus iso-prod actuellement)

Sous-taches:
 - Lister les tickets ayant eu un impact sur la mire depuis la dernière tentatives de livraison 
 - Tirer une nouvelle branche de release de la mire depuis le master des projets concernés
 - Changer les features flag de cette branche de release  pour n'activer que la mire et la redirection vers le légacy
 - Tagger une version
 - Tester cette version en pilote (notre environnement actuel le plus proche de la prod)
   - Tests fonctionnels
   - Tests de charge sur pilote. (nous avions déjà des tests avec artillery de prêt)