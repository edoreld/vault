# Appel fund_transfer_option pour contrôle et récupération du type de virement

User Story: https://jira-agile.fr.cly:8443/browse/RST-887

 - [ ] Waiting for feedback on PR

When we add a beneficiary, we go directly to the add amount stage. Therefore, we need to know the transfer type options for this account so that we can show the right options.

My task is as follows:

Whatever means we alreay use when checking when a user selects a beneficiary account and goes to the next step, we need to also use when a user adds a new beneficiary and then goes directly to the amount input page. 

The gist of it is, when we show beneficiary accounts, the beneficiary accounts already have the info of which transfer types are allowed. When we add a new beneficiary account, we need to do the same validation and return this information to the front (?)

The endpoint I will develop will do two things: 
 - It will **validate the IBAN** and **return the options for the iban**. It does both of this things through calling /funds_transfer_options.

I need to choose 

Description:
```
Si tu veux vérifier un IBAN bénéficiaire en terme structure d’IBAN, atteignablilité  IP de la banque, et de caractère Créditable ou non pour les IBAN LCL, tu peux utiliser Funds_transfer_options.

Tu renseignes l’IBAN dans la partie Creditors

Le type de virement correspond à VIO ou VII pour savoir quel type de virement on va pouvoir faire 
```

We can indeed use funds_transfer_options to verify an Iban's transfer type, but we need to put in a debtor and a list of creditors (with just the one entry) in order to do so.

