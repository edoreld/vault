# Point Tests de Charge

Comment standardiser les tests de charge lors de nos déploiement  ?

 - Pourquoi ?

Contexte: 
 - mise en prod de la MEP
 - pas des tests en charge
 - on depend de beaucoup de services
 - Si l'un de ces services tombe en panne, nous on tombe aussi
 - Problème de timeouts -> réglage de timeouts
 - Qu'est-ce que on a fait -> tester individuellement chaque service
	 - Même charge mais dans un env iso-prod
 - Tests de charge faits par une autre equipe de LCL hors iso-prod

Deux types de tests de charge:
 1. On isole nos services et on dimensionne nos services selon la charge attendue
 2. On teste les services dont nous on depend

**Tests de perf** : performance du code que on a produit
	- Tools:
		- JMH
		
**Tests de charge**: 
 - Arguments against:
	 - intérêt à le faire isolé des autres ?
	 - env recette != env prod, tests non applicables ?


Il faut essayer d'avoir un environnement de test aussi proche de la production que possible afin de na pas biaser ses resultats

Environnement dedié aux tests de charge ?

Mêmes composants en pilote et prod, si pilote tombe, prod aussi

Déploiement multiple-service

Observabilité

On regarde avec nos yeux, mais avant de rentrer une pièce on sait pas qu'est-ce que on va regarder. 

 - Metrologie (métrique): 
	 - consommation cpu de mon application à un instantané ? 
	 - tools: _Prometheus_, _InfluxDB_, etc
 - Centralisation de logs: quoi mettre dans le logs ? 
 - Tracing: 
	 - i.e : combien de temps est passé entre les différent composants pour la résolution d'une reqûete ?
	 - APM: application performance manager -> observer le comportement d'un composant dans le cas d'un test de charge != pas de monitoring en prod
	 - tools: tracing

Injecteur -> machine qui va tirer sur l'API
	- Bien d'en avoir plusieurs 
	- Situé si possible à l'exterieur (comme l'utilisateur final)

 - **tools**
	- gatling (?)
	- artillery 
	- j-emitter (?)

Span = window of code between X and Y 

Metriques custom avec dynatrace possibles, il faut demander un token Dynatrace 


## Ideas	
 - Mettre en place un métrologie / Racolter des infos sur l'existant 
 - A transversal team that handles tests de charge for all teams?
 - Is it worth it to test only one our side?
 - decoupling services when doing tests de charge risks not capturing the problems in the interactions between services
 - Canary tests on transfer part only


fichier: override

