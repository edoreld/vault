# Impulses

## List of impulses 16/06/2020
 - [ ] [14:30] Visit Reddit on non-work computer <- Triggered on figuring out what is the token in BCT request
 - [ ] [14:35] Open Slack
 - [ ] 

## List of impulses 15/06/2020 
N/A

## List of impulses 14/06/2020 
 - [x] Search Amazon for a rice, brown rice and quinoa cooker
 - [x] Open Software Update panel to check the status of the OS update
 - [x] Set up a git repo for this vault so I can access the impulses at work 
 - [x] Look up "Ego Depletion"
 - [ ] Go to "Twitch.tv" and browse Dota channels
 - [x ] !g "risk mangement possible responses"
 - [ ] Browse Reddit <- triggered on realizing the gap in my knowledge in the Goban problem 
 - [x] Browse Reddit
 - [ ] Browse Twitch
 - [ ] Browse Reddit
 - [x] Browse Reddit
 - [x] Browse Twitch
 - [x] Browser Hacker news 


