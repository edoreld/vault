# Ne pas afficher de plafond de virement compte à compte

Transfer-Service à tester: 
 - https://review-rst-1093-h-itjawl-transfer-transac.hors-prod.caas.lcl.gca

Transac-Frontend à tester
 - https://review-rst-1093-h-l1am04-frontend-transac.hors-prod.caas.lcl.gca

Rétablir Transfer-Service sur WS02 Publisher

https://integration-transfer-transac.hors-prod.caas.lcl.gca
https://integration-transfer-transac.hors-prod.caas.lcl.gca

OK
```
Cas 1 : Compte à compte

Se connecter avec John Tibi 7693355176 / 224510

Faire un virement

depuis : Compte dépots John

vers : Plan épargne logement

Montant : On ne doit pas voir de message sous le champ

Saisir 100 000€, un message doit s'afficher pour prévenir qu'on dépasse le solde

Saisir 56 € et valider le virement jusqu'au bout
```
 
Ok
```
Cas 2 : Compte LCL vers externe (non régression )

Se connecter avec John Tibi 7693355176 / 224510

Faire un virement

depuis : Compte dépots John

vers : magic

Le message sous le montant est affiché

Saisir 12€ et terminer le virement

Recommencer et voir que le plafond est mis à jour

 ```
 
KO
 - [ ] Fix red highlight on montant when virement > ceiling in internal transfers
```
Cas 3 : Compte à compte

Se connecter avec John Tibi 7693355176 / 224510

Faire un virement

depuis : Compte dépots John

vers : Plan épargne logement

Montant : On ne doit pas voir de message sous le champ

Saisir 5000€, le virement doit passer car il n'y a pas de limite en dehors du solde + découvert
```