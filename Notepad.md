# Notepad

Cuisine 
Salle de bain à 

####
Price: 130300
Link: 
 - [link](https://www.kwfrance.com/fiches/3-33-29_36887160/appartement-ris-orangis-3-piece-s-57-70-m2.html)
 - [link](https://www.bienici.com/annonce/vente/ris-orangis/appartement/3pieces/immo-facile-36887160)
Size: 57m<sup>2</sup>
Action: call again later

 - Quoi n'est pas couvert par les charges ?
	 - Electricité
 - A quelle distance en minutes de la gare ?
	 - 10m grigny rer
 - Chauffage
	 - Collectif
 - Calme ?
	- Calme selon agent
 - Secteur
 - Vitrage ?
	 - 
 - Travaux à prévoir ?
	 - 
 - Meublé ou pas ?
	- 
Bonjour Nicolas,

J'ai regardé hier la page sur la bobelisation et j'avais commencé à travailler sur la requête qui est sous "Bobélisation des virements". Or, Anne-Sophie nous dit que cette requête est utilisée pour un virement entre amis. 

Eric de mon équipe me dit que tu avais déjà développé la route pour la bobelisation d'un virement occassionel, mais je la vois pas dans la page confluence. Peut tu me renseigner :-) ?

```json
[{
    "iban": "FR3430002003150000017203D04",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }]
}, {
    "iban": "FR6530002003870000017002Y92",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }]
}, {
    "iban": "FR5730002003150000015105Z73",
    "options": null
}, {
    "iban": "FR3730002003150000013910Z33",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }]
}, {
    "iban": "FR7630004000031234567890143",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }, {
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VII",
        "timeLimit": 3
    }]
}, {
    "iban": "FR7630041000010000000002275",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }, {
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VII",
        "timeLimit": 3
    }]
}, {
    "iban": "FR7612548029981234567890161",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }, {
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VII",
        "timeLimit": 3
    }]
}, {
    "iban": "FR7630041000010000000006058",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }, {
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VII",
        "timeLimit": 3
    }]
}, {
    "iban": "FR7630001007941234567890185",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }, {
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VII",
        "timeLimit": 3
    }]
}, {
    "iban": "FR7630041000010000000000335",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }, {
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VII",
        "timeLimit": 3
    }]
}, {
    "iban": "FR7611315000011234567890138",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }, {
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VII",
        "timeLimit": 3
    }]
}, {
    "iban": "FR3230002002220000375865J12",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }]
}, {
    "iban": "FR8230002003810000368312Y29",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }]
}, {
    "iban": "FR7630004001250000022073536",
    "options": [{
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VIO",
        "timeLimit": 3
    }, {
        "usage": "IBN",
        "fee": 0.0,
        "ceiling": 50000,
        "transferType": "VII",
        "timeLimit": 3
    }]
}]
```
