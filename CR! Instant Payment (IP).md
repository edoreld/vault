# CR! Instant Payment (IP)

**Object** : comment avertir le client en cas d’émission d’Instant Payment rejeté. 
**Participants**: Javier, Gaëlle, MASUREL Sandrine, Julien, Eric, Roy Laetitia, Baptiste Derymacker
**Link (more recent version)**: https://confluence.fr.cly:8443/pages/viewpage.action?pageId=81527325


## Notes

Client mobile valide opération avec AF,  on le renvoie une notification qui est le résultat de l'opération.

Notification WSPEC envoyée vie l'appli mobile

Notification WSPEC "Votre beneficiare a recu l'argent" ou "problème lors de l'envoie"

WSPEC peut faire SMS, e-mails. Éventuellement ils pourraient alimenter la base d'alerte. 

Besoin de garder captive pour notifier le client du résultat de l'operation, surtout si l'opération finale n'aboutit pas. 

Filtres posibles d'opération: 
Si operation vient de mobile, favoriser notification push.
Si opération vient de deskop, favoriser sms.
**Favoriser la solution avec le meilleur équilibre entre le comfort du client et la minimisation du coût**

Étude de solution à faire
 
 
