# CR! Point Bugs Virement
Updated version at: https://confluence.fr.cly:8443/display/RST/CR+Point+Bugs+Virement

## Cas :

## Bug et Actions Prises

### One
 - Constat: Lenteur pour voir apparaître la liste de débiteurs, dans synthèse et virements
 - Pistes: côté datalayer ?
 - Action: @Gaelle => **créer ticket pour analyser**

### Two
 - Aucun compte débiteur s'affiche pour le cas "Patrice Lelièvre"
 - Pistes: possible erreur avec env de recette
 - **Résolution**: env dev marche, c'était rct qui avait des problèmes

### Three

Cas: **Patrice Lelièvre**

 - Constat: pas de comptes internes affiché depuis mercredi
 - Pistes: possible régression
 - Pistes: des nombreux erreurs liés aux comptes internes liés à la réponse de funds_transfer_options
	 - EMISSION CPT BEN PAS AUTORISEE PAR CPT DO
 - Action: @Javier => investiguer les commits du mercredi et voir si un commit a introduit la régression
 - Action: @Gaëlle => contacter MPA 

### Four

 - Constat: pas de nombres pour les comptes récupéres de la BCT
 - Action: @Javier vérifier la réponse de l'appel et contacter BCT si besoin


