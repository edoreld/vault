# Meditation can help us overcome the lack of self-awareness
#meditation #addiction #self-control 

Meditation is the practice of quiet contemplation of our own mind.

Meditation trains us to be aware of our own irrational thoughts and impulses([[Rational vs Irrational MInd]]) and, in turn, help us change our irrational behavior before it happens.




