# See why transfer-service is not returning internal accounts

**To get info on accounts**

Swagger: in Internet Explorer [this url](http://dvl-doc-datalayer.lclhp-ee.docker.prodinfo.gca/#/alpha/get_comptes) 

## Test Cases

John TIBI

idReper: 7514224510
idBel: 7693355176

(For Swagger):
encodedIdReper: NzUxNDIyNDUxMA
encodedIdBct: MDAzMTMwMzUwODA2NDcyMw

(For calling "creditor_accounts")
user: 7693355176
pass: 224510
contract: 0031303508064723
encodedIban: RlI1NDMwMDAyMDAxMDIwMDAwMDI1ODcyWDcz




Old site for reference: https://ecl-rec5-rp-was7.id.fr.cly/outil/UAUT?from=/outil/UWHO

## Notes
- We get a new error code for internal accounts "EMISSION CPT BEN PAS AUTORISEE PAR CPT DO "
- Sent email and waiting for response from the team

## Comparing the list of accounts from swagger to list of accounts from accounts-service

https://integration-accounts-transac.hors-prod.caas.lcl.gca/accounts?contract_id=0031303508064723

## Internal Accounts recovered from Transfer service
```json
[{
    "iban": "FR5430002001020000025872X73",
    "can_choose_instant_payment": null,
    "instant_payment_fee": null,
    "id": "XzAwMTAyMDI1ODcyWA",
    "label": "Compte de dépôts",
    "correspondent_label": "M JOHN TIBI",
    "is_credit_allowed": false,
    "is_debit_allowed": false,
    "external_id": "00102 025872X",
    "balance": 4190.00
}, {
    "iban": "FR3430002003150000017203D04",
    "can_choose_instant_payment": null,
    "instant_payment_fee": null,
    "id": "XzAwMzE1MDE3MjAzRA",
    "label": "Compte dépots John",
    "correspondent_label": "M JOHN TIBI",
    "is_credit_allowed": false,
    "is_debit_allowed": false,
    "external_id": "00315 017203D",
    "balance": 86327.10
}, {
    "iban": "FR6530002003870000017002Y92",
    "can_choose_instant_payment": null,
    "instant_payment_fee": null,
    "id": "XzAwMzg3MDE3MDAyWQ",
    "label": "Compte de Partic23 dépôts",
    "correspondent_label": "MONSIEUR VINGTTROI PARTIC",
    "is_credit_allowed": false,
    "is_debit_allowed": false,
    "external_id": "00387 017002Y",
    "balance": 800.15
}, {
    "iban": "FR1930002003150000870154K80",
    "can_choose_instant_payment": null,
    "instant_payment_fee": null,
    "id": "XzAwMzE1ODcwMTU0Sw",
    "label": "Livret A",
    "correspondent_label": "M JOHN TIBI",
    "is_credit_allowed": false,
    "is_debit_allowed": false,
    "external_id": "00315 870154K",
    "balance": 100.00
}, {
    "iban": "FR0930002001020000367703Z74",
    "can_choose_instant_payment": null,
    "instant_payment_fee": null,
    "id": "XzAwMTAyMzY3NzAzWg",
    "label": "Livret Dév. Durable et Solidaire",
    "correspondent_label": "M JOHN TIBI",
    "is_credit_allowed": false,
    "is_debit_allowed": false,
    "external_id": "00102 367703Z",
    "balance": 10.00
}, {
    "iban": "FR3130002001020000202502L04",
    "can_choose_instant_payment": null,
    "instant_payment_fee": null,
    "id": "XzAwMTAyMjAyNTAyTA",
    "label": "Compte sur livret",
    "correspondent_label": "M JOHN TIBI",
    "is_credit_allowed": false,
    "is_debit_allowed": false,
    "external_id": "00102 202502L",
    "balance": 100.00
}, {
    "iban": "FR8330002001020000202505P96",
    "can_choose_instant_payment": null,
    "instant_payment_fee": null,
    "id": "XzAwMTAyMjAyNTA1UA",
    "label": "Compte sur livret",
    "correspondent_label": "M JOHN TIBI",
    "is_credit_allowed": false,
    "is_debit_allowed": false,
    "external_id": "00102 202505P",
    "balance": 10.00
}, {
    "iban": "FR5730002003150000015105Z73",
    "can_choose_instant_payment": null,
    "instant_payment_fee": null,
    "id": "XzAwMzE1MDE1MTA1Wg",
    "label": "Plan d'épargne logement",
    "correspondent_label": "M JOHN TIBI",
    "is_credit_allowed": false,
    "is_debit_allowed": false,
    "external_id": "00315 015105Z",
    "balance": 0.00
}, {
    "iban": "FR3730002003150000013910Z33",
    "can_choose_instant_payment": null,
    "instant_payment_fee": null,
    "id": "XzAwMzE1MDEzOTEwWg",
    "label": "Compte épargne logement",
    "correspondent_label": "M JOHN TIBI",
    "is_credit_allowed": false,
    "is_debit_allowed": false,
    "external_id": "00315 013910Z",
    "balance": 710.80
}]

```

## Accounts recovered from John Tibi case

```json
[
  {
    "id": "XzAwMzg3MDE3MDAyWQ",
    "connexion": {
      "id": "LCL",
      "libelle": "LCL"
    },
    "numhrscrt": "003870000017002",
    "code_regroupement": "CD",
    "codfctttr": "4",
    "code_banque": "30002",
    "id_compte": {
      "code_agence": "00387",
      "numero_compte": "017002",
      "lettre_cle": "Y"
    },
    "rib": "30002003870000017002Y92",
    "iban": "FR6530002003870000017002Y92",
    "datcreacpt": "2004-12-01T10:29:15.721+0000",
    "activite": {
      "codetacrt": 1,
      "libetacrt": "Actif"
    },
    "produit": {
      "codfmtprd": "0006",
      "libcouprdcml": "DEPOTS",
      "liblonprdcml": "Compte de Partic23 dépôts"
    },
    "titulaire": {
      "libittdendst": ""
    },
    "nad": "006",
    "id_groupe": "145695",
    "participations": {
      "9500001038": {
        "codptpcrt": "00",
        "libptpcrt": "TITULAIRE",
        "debut": "2001-01-01T00:00:00.000+0000",
        "fin": "2099-12-01T00:00:00.000+0000"
      }
    },
    "soldes": {
      "COMPTABLE_ESPECE": {
        "natursolde": "00",
        "date": "2018-11-15T00:00:00.000+0000",
        "montant": {
          "valeur": 80015,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      },
      "COMPTABLE_PROVISOIRE": {
        "date": "2020-06-19T08:55:31.409+0000",
        "montant": {
          "valeur": 80015,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      },
      "COMPTABLE_PROVISOIRE_DEVISE": {
        "date": "2020-06-19T08:55:31.409+0000",
        "montant": {
          "valeur": 80015,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      }
    },
    "top_paylib": false
  },
  {
    "id": "XzAwMzE1MDE3MjAzRA",
    "connexion": {
      "id": "LCL",
      "libelle": "LCL"
    },
    "numhrscrt": "003150000017203",
    "code_regroupement": "CD",
    "codfctttr": "4",
    "code_banque": "30002",
    "id_compte": {
      "code_agence": "00315",
      "numero_compte": "017203",
      "lettre_cle": "D"
    },
    "rib": "30002003150000017203D04",
    "iban": "FR3430002003150000017203D04",
    "datcreacpt": "2010-04-12T17:29:13.530+0000",
    "activite": {
      "codetacrt": 1,
      "libetacrt": "Actif"
    },
    "produit": {
      "codfmtprd": "0006",
      "libcouprdcml": "DEPOTS",
      "liblonprdcml": "Compte dépots John"
    },
    "titulaire": {
      "libittdendst": ""
    },
    "nad": "006",
    "id_groupe": "145695",
    "participations": {
      "7514224510": {
        "codptpcrt": "00",
        "libptpcrt": "TITULAIRE",
        "debut": "2001-01-01T00:00:00.000+0000",
        "fin": "2099-12-01T00:00:00.000+0000"
      }
    },
    "soldes": {
      "COMPTABLE_ESPECE": {
        "natursolde": "00",
        "date": "2018-11-15T00:00:00.000+0000",
        "montant": {
          "valeur": 10030000,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      },
      "COMPTABLE_PROVISOIRE": {
        "date": "2020-06-19T08:55:31.409+0000",
        "montant": {
          "valeur": 8632710,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      },
      "COMPTABLE_PROVISOIRE_DEVISE": {
        "date": "2020-06-19T08:55:31.409+0000",
        "montant": {
          "valeur": 8632710,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      }
    },
    "top_paylib": false
  },
  {
    "id": "XzAwMzE1MDEzOTEwWg",
    "connexion": {
      "id": "LCL",
      "libelle": "LCL"
    },
    "numhrscrt": "003150000013910",
    "code_regroupement": "CE",
    "codfctttr": "3",
    "code_banque": "30002",
    "id_compte": {
      "code_agence": "00315",
      "numero_compte": "013910",
      "lettre_cle": "Z"
    },
    "rib": "30002003150000013910Z33",
    "iban": "FR3730002003150000013910Z33",
    "datcreacpt": "2005-07-28T11:58:14.203+0000",
    "activite": {
      "codetacrt": 1,
      "libetacrt": "Actif"
    },
    "produit": {
      "codfmtprd": "0007",
      "libcouprdcml": "CEL",
      "liblonprdcml": "Compte épargne logement"
    },
    "titulaire": {
      "libittdendst": ""
    },
    "nad": "007",
    "id_groupe": "145696",
    "participations": {
      "7514224510": {
        "codptpcrt": "00",
        "libptpcrt": "TITULAIRE",
        "debut": "2001-01-01T00:00:00.000+0000",
        "fin": "2099-12-01T00:00:00.000+0000"
      }
    },
    "soldes": {
      "COMPTABLE_ESPECE": {
        "natursolde": "00",
        "date": "2018-11-15T00:00:00.000+0000",
        "montant": {
          "valeur": 658,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      },
      "COMPTABLE_PROVISOIRE": {
        "date": "2020-06-19T08:55:31.409+0000",
        "montant": {
          "valeur": 71080,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      },
      "COMPTABLE_PROVISOIRE_DEVISE": {
        "date": "2020-06-19T08:55:31.409+0000",
        "montant": {
          "valeur": 71080,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      }
    },
    "top_paylib": false
  },
  {
    "id": "XzAwMzE1MDE1MTA1Wg",
    "connexion": {
      "id": "LCL",
      "libelle": "LCL"
    },
    "numhrscrt": "003150000015105",
    "code_regroupement": "CE",
    "codfctttr": "3",
    "code_banque": "30002",
    "id_compte": {
      "code_agence": "00315",
      "numero_compte": "015105",
      "lettre_cle": "Z"
    },
    "rib": "30002003150000015105Z73",
    "iban": "FR5730002003150000015105Z73",
    "datcreacpt": "2016-01-13T10:48:47.294+0000",
    "activite": {
      "codetacrt": 1,
      "libetacrt": "Actif"
    },
    "produit": {
      "codfmtprd": "0012",
      "libcouprdcml": "PEL",
      "liblonprdcml": "Plan d'épargne logement"
    },
    "titulaire": {
      "libittdendst": ""
    },
    "nad": "012",
    "id_groupe": "145696",
    "participations": {
      "7514224510": {
        "codptpcrt": "00",
        "libptpcrt": "TITULAIRE",
        "debut": "2001-01-01T00:00:00.000+0000",
        "fin": "2099-12-01T00:00:00.000+0000"
      }
    },
    "soldes": {
      "COMPTABLE_ESPECE": {
        "natursolde": "00",
        "date": "2018-11-15T00:00:00.000+0000",
        "montant": {
          "valeur": 0,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      },
      "COMPTABLE_PROVISOIRE": {
        "date": "2020-06-19T08:55:31.409+0000",
        "montant": {
          "valeur": 0,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      },
      "COMPTABLE_PROVISOIRE_DEVISE": {
        "date": "2020-06-19T08:55:31.409+0000",
        "montant": {
          "valeur": 0,
          "devise": {
            "ninterdevi": 978,
            "coddevitn": "EUR",
            "decdevitn": 2
          }
        }
      }
    },
    "top_paylib": false
  }
]
```