# Neofit Rilski

Who is Neofit Rilski really?

The real name of Neofit Rilski is Nikola Poppetrov Benin and he is a famous Bulgarian Revival writer and enlightener with diverse interests who was both a monk, a teacher and an artist, an architect, a musician, a poet, which makes him one of the patriarchs of the Bulgarian enlightenment. He is the founder of the Bulgarian secular education and the first Bulgarian encyclopedist.

This prominent writer of the Bulgarian national revival created the first Bulgarian grammar book in 1835. The Bulgarian Konstantin Irechek described him as “patriarch of the Bulgarian teachers and scholars“. He was born in 1793 and died in 1811 as a descendant of merchants, diplomats.

He taught iconography at the Art School of Toma Vishanov-Molera in Bansko. He worked in the Rila Monastery, where he later became a monk. His birthplace in Bansko was transformed into Neofit Rilski House-Museum also known as Benina House. He lived there until 1811.

The famous writer of the Bulgarian national revival studied icon painting with the founder of the Bansko Art School, Toma Molera. He painted at Rila Monastery where he later became a monk and also an abbot of the monastery later. Throughout his life, Neofit Rilski is committed to progress.

Here is what his final words were before his death sounded like, which have turned into winged words:

“I am not afraid of death itself, but I am afraid that there will still be many unfinished things that are necessary for the enlightenment of the Bulgarians.”