git checkout  -b "RST-984/group_logs"

deployment.yaml

  metadata:
      annotations:
        co.elastic.logs/multiline.pattern: "^time="
        co.elastic.logs/multiline.negate: "true"
        co.elastic.logs/multiline.match: "after"

git add *

git commit -m "group logs"

git push --set-upstream origin RST-984/group_logs

https://jira-agile.fr.cly:8443/browse/RST-984

 - [x] transfer-service
 - [x] account-service
 - [x] contract-service 

 - [x] user-service
 - [x] warbel-context-provider
 - [x] authentication-service
 
 
Group logs on Kibana:
 - [ ] transfer-service: https://scm.saas.cagip.group.gca/lcl/bpi/transac/belpp-03631-metier/transfer-service/merge_requests/86
 - [ ] account-service: https://scm.saas.cagip.group.gca/lcl/bpi/transac/belpp-03631-metier/account-service/merge_requests/54
 - [ ] contract-service: https://scm.saas.cagip.group.gca/lcl/bpi/transac/cntbel-03823-metier/contract-service/merge_requests/48
 - [ ] user-service: https://scm.saas.cagip.group.gca/lcl/bpi/transac/belpp-03631-metier/user-service/merge_requests
 - [ ]  authentication-service: https://scm.saas.cagip.group.gca/lcl/bpi/transac/iact-03642-metier/authentication-service/merge_requests/104
 - [ ]  warbel-context-provider: https://scm.saas.cagip.group.gca/lcl/bpi/transac/iact-03642-metier/warbel-context-provider/merge_requests