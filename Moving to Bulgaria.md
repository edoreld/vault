# Moving to Bulgaria
As a result of a shrinking economy, Bulgaria has extremely cheap properties.

In an area called Bansko, you can get a fully-furnished appartment for less than 20,000€. 


## Buying an appartment 

The things to watch out for:
 - Maintenance fees
 - Heating (mostly electric and not great insulation)
 - Any issues with permits (Act 16) or pending legal action against initial developer (a lot of them went bankrupt, so no warranties)
 - Is there a rental scheme for them complex that allows you to rent out your apartment? If so, how much money do the other owners make with that scheme (generally best to not plan on any rental income from your apartment as there is a lot of competition that drives down prices and a lot of regulation for short term rentals)
 - What facilities are in the complex or nearby? Open year-round or only during the winter season?

## Getting Residency 
Once you have an apartment and a company, you can go to the local immigration office in the Bulgarian city where you want to register to get your residency. In Blagoevgrad, the office is located at street Vlado Chernozemsky 3.

## What do do

Resources: 
 - [A Digital Nomad’s Guide to Bansko - Nav Aulakh - Medium](https://medium.com/@navaulakh/a-digital-nomads-guide-to-bansko-9e0b4e09d1c2)

## What do do in the summer

https://asocialnomad.com/bulgaria/bansko-summer/

## Learn about Bansko
 - [Traveling with Kristin — Digital Nomad Blog — My €150/Month Digital Nomad Apartment in Bulgaria](https://www.travelingwithkristin.com/digital-nomad-blog/2019/1/23/my-150month-digital-nomad-apartment-in-bulgaria)

## Tax

 - [Getting Bulgarian tax residency for digital nomads - Coworking Bansko - Medium](https://medium.com/@coworkingbansko/getting-bulgarian-tax-residency-for-digital-nomads-6e6a71b51ff0)

## Real State Sites

 - [Bulgarian Property for Sale and Rent | Excel Property Bulgaria](https://excelpropertybulgaria.com/)
 - (Rent only) [Bansko Nomad Apartments - Monthly Rentals for the Coworking Bansko Community](https://www.banskonomadapartments.com/)
 - [Real estate for sale in Bulgaria. Property in cities, towns, villages, resorts for sale.](https://www.bulgarianproperties.com/Search/index.php?stip=0&stop=0&stown=7&sdevv=&maxprice=&skeyword=&sadx=1&spredlog=innear&c=Search&srtby=2)
 - [Bulgarian Properties for sale and rent - buy houses in Bulgaria , apartments and flats, land offices, rural property and hotels](https://www.bulgarianproperties.com/)

## Where to stay?


## Lawyer Services
http://banskolawyer.com/




Contacts:
 - margi3@mail.bg. She’s very well connected in the area and will help you get really good deals for a good quality apartment/room at the local rate (not the tourist rate).
 - d.dzhibin@gmail.com (Local Property Contact FB page)
 - bestpropertyshow@gmail.com (Ask for Sergey)