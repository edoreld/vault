# Draft

Bonjour Anne-Sophie, comment vas-tu ?

Nous sommes en train de travailler sur la mise à jour du plafond glissant lors d'un virement.

Selon mes emails, pour Paylib ça passe par la transaction E6PM. Je me demande comment on devrait faire pour les virements non-Paylib. Y a-t-il un endpoint existant qu'on peut regarder (soit un swagger, soit du code source) ?


Hello Abdessamie

 Je suis en train de travailler sur la mise à jour du plafond glissant lors d'un virement. Je crois que ça passe par le service MSPLFBEL et l'appel soit à E6PI, E6PM ou E6PY.
 
 J'ai trouvé le repo de MSPLBEL sur Github et je vois que tu est le contributeur le plus actif. Je me demande si tu connais les URLs à appeler pour accèder à ce service ?
 
 Dans notre équipe on a l'URL "http://dvl1-playlib-plfbel.lclhp-ee.docker.prodinfo.gca/msplfbel/" mais je reçois toujours une erreur 503. 
