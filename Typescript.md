# Typescript

A superset of JavaScript which adds interfaces, classes and better type verification. It compiles to valid JavaScript.

## Type Assertion

Type assertion is sort of like putting a data type between brackets in front of a variable name. It forces the variable to be treated as a certain type.

```ts
let code: any = 123; 
let employeeCode = code as number;
```

Code is of type any
`employeeCode` is asserted to be of type `number`

## Optional Parameters 
Putting a question mark at the end of a parameter makes it optional

```javascript
function buildName(firstName: string, lastName?: string) {
```

lastName is optional. The user can put it or not. Optional parameters must come at the end of a method.