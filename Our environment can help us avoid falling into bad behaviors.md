# Our environment can help us avoid falling into bad behaviors

It can help to separate our work environment from our work-out environment from our sleeping environment. Having a mixed environment is a cognitive load that erases our willpower. 

More options lead to more impulses which lead to faster [[Ego depletion]]. That's why the Internet is so dangerous. At any given time there are infinite things we could be doing on it. Having access to the Internet can lead to a constant stream of impulses that have to be dealt with somehow. See [[Ways to manage ego depletion when on the computer]]


