# Work Log
 ## funds-transfer
  - Helping out Guillaume with Swagger and showing him how to call funds_transfer
  - Removed unimplemented field to unblock PO testing
  - Implementing verification of input data
  - Implementing accepting encoded data

## KPI

Number of Code Reviews proposed:
 - 1: Jimmy | Bobel -> Done
 - 1: made myself available in Slack

Number of # asked for help:
 - 1: Wenwu
 - 1: Guillaume

Shared at daily, self-rating:
 - 04/08: 5
 - 05/08: 4 (could have been 5 if I had been me to mention first that I was gonna work on next sprint's subjects)

## August 17, 2020

 - Verification pays autorisés (2 tests to fix)

RST-972 : validation montant contre plafond et decouvert autorisé et blocage de transfert si échoue

RST-887: validation et récupération pour un iban lors de l'ajout d'un beneficiaire 

## August 4, 2020


## August 3, 2020

 - Contacted Anne-Sophie regarding the updating of the rolling ceiling

```
Bonjour Anne-Sophie, comment vas-tu ?

Nous sommes en train de travailler sur la mise à jour du plafond glissant lors d'un virement.

Selon mes emails, pour Paylib ça passe par la transaction E6PM. Je me demande comment on devrait faire pour les virements non-Paylib. Y a-t-il un endpoint existant qu'on peut regarder (soit un swagger, soit du code source) ? 
```



Contacté Abdessamie à propos de l'URL pour appelerMSPLFBEL

Blocked on -> Unblocked!
```
 - Can't test MSPLFBEL because endpoint unavailable
```

Contacté Pôle Web Tech pour remettre service MSPLFBEL

Contacté plusiers personnes pour avoir des requêtes d'exemple vers MSPLFBEL -> Obtained!

Started work on coding part

TODO: 
 - Tests for code
 - Think about Amount's Big Decimal to Int conversion

 - Found the code
 - Wanted to test...
 - Contacted Team
 - Tested successfully with Postman
 - Now implementing in code

http://api.msplfbel.lcl.fr/plafond/null?codeAgence=some-agency-id&codeCompte=some-account-id


## July 6th, 2020

Étant en vacances jusqu'àu 1er août, voici mes notes pour le daily. 

 -  PR pour funds_transfer (en attente de pouce) pour : 
	 -  Valoriser nom de débiteur et du créditeur
	 -  Enlever des appels inutiles (on a passé de 6.33s à 4.03 secondes pour l'appel à funds_transfer)
 - PR mergée => Remove iban and bic from our code
 - PR mergée => accepter iban et bic encodés 
 
## July 4th, 2020

 - Mr to => Removed calls to funds_transfer_options and reused some requests. Time to call funds_transfer on our side went down 2.30s from 6.33s to 4.03s)
 - MR & merge => Remove iban and bic from our code
 - Finished merging MR to accept encoded iban and contract id
 - Worked on the code for taking into account correspondent_label


## July 3rd, 2020
 - Helping out Guillaume with Swagger and showing him how to call funds_transfer
 - Create branch with removed unimplemented field to unblock PO testing
 - Implementing verification of input data
 - Implementing accepting encoded data

## Can work on
Using actual data instead of placeholders on funds-transfer.
See what data we already recover from `debtor_accounts` and `creditor_accounts`
 
