# Glossary

**Jest**
A JavaScript testing framework

**Modal**
A Modal is a dialog that appears on top of the app's content, and must be dismissed by the app before interaction can resume

