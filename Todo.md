# TODO

 - [ ] [[Authentication Forte]] 
 - [ ] [ ] Visit https://etoro.tw/34AusZ4
 - [ ] #waiting [[country check verification]]
 - [ ] [[Bloop virement instantané]]
 - [ ] Verify why https://jira-agile.fr.cly:8443/browse/RST-998 is not delivered on pilote
 - [ ] Integrate RST-1093 en pilote
 - [ ] Merge double Bobel into new repo [MR](https://scm.saas.cagip.group.gca/lcl/bpi/transac/belpp-02262-ui/transac-frontend/merge_requests/32)
 - [ ] Merge hide ceiling [MR](https://scm.saas.cagip.group.gca/lcl/bpi/transac/belpp-02262-ui/transac-frontend/merge_requests/20)


[[Test Cases]]
 - [x] [[Ne pas afficher de plafond de virement compte à compte]]
 - [x] Re-publish remove-test and re-test
 - [x] Publish PRs for hide ceiling/etc
 - [x] Fix not all accounts showing on liste beneficiares
 - [x] Make " Le montant de votre virement ne peut excéder 1 000,00 €", be the minimum of ceiling | overdraft for external transfers

Transfer-Service à tester: 
 - https://review-rst-842-bl-t4d0a5-transfer-transac.hors-prod.caas.lcl.gca

Transac-Frontend à tester
 - https://review-front-rst-bjhgkb-frontend-transac.hors-prod.caas.lcl.gca


Rétablir Transfer-Service sur WS02 Publisher

https://integration-transfer-transac.hors-prod.caas.lcl.gca
https://integration-transfer-transac.hors-prod.caas.lcl.gca

When setting .nav's position to 'relative', the link display is fixed, but the rest of the page is fucked up

[[Mocking the back on transac-frontend]]

## 21/09/2020

 - [ ] When doing an internal transfer, compare only decouvert, and not ceiling, on back
 - [x] Fix Title display bug on confirmation page 
 - [ ] Fix 3rd cas de test on hide title story
 - [x] When internal transfer && transferAmount > decouvert -> show "Votre compte ne présente pas un solde suffisant" 

```java
case "B0065":
case "I0005":
title = "";
message = "Votre compte ne présente pas un solde suffisant.";
```

**CLI**
7693355176
963767

**CLA**
1556658732
811700
 
 
**John Tibi**
7693355176
 224510
 
 
 xebia2012 
log : lcl@xebia.f## 

## 18/09/2020

- [ ] [[Hide transfer message between two accounts]]

## 17/09/2020
CAN DO BEFORE DEMO


TODO AFTER DEMO
 - [x] Change IDENTITY_PROVIDER_URL in gitlab config in transac
 - [x] Relaunch pilote
 - [x] Test login

Login doesn't work and we get a BACK_UNREACHABLE error. This error is usually generated when there is a problem with the gateway.

## 04/09/2020

 - [x] merge common-jwt
	 - [ ] merge account-service 

contract_id_modifiable
 - PR common-jwt to add verification: need to treat feedback and add test
 
 utiliser PreAuthorise
 - PR in contract-id to use common-jwt PR to verify input

Show correct transfer-ceiling
 - In PR

Front popin saisie d'iban
 - On a avance ensemble avec Antoine, ça ressemble plus à l'objectif 
 -  Mask?
 -  


 - [x] Look at WenWu's common-jwt PR
 - [x] Look at Ked Stuff 
 - [ ]  Ask for review on reviews

## 03/09/2020
 
 - [ ] Make transfer-service return decimals instead of cents
 - [ ] Lack PC for remote work
 - [ ]  Spend 10 minutes at the end of the day writing about what you accomplished and preparing tomorrow's daily
 
 ## 02/09/2020
 - [ ]
 - [ ] Center title on recapitulatif page (transac-frontend)
 - [x] Ask Rami why if contract id is null we still return `cards` 
 - [ ] Hide `Ajouter un bénéficiaire` when passing to next step (like with amount/plafond step)






## Done
 - [x] [[Vérifier appel à la BCT]]  ([RST-728](https://jira-agile.fr.cly:8443/browse/RST-728))
 - [x] [[Développer un endpoint dans transfer-service pour executer le virement]] ([RST-803](https://jira-agile.fr.cly:8443/browse/RST-803))
 - [x] [[Add IT to RST-803]]
 - [x] [[Fix deployment bug]]
 - [x] Send reminders to KED presenters, something like this:
	 > Hello @annesophiegiraultlemault,
	 > Comme en juin, le KED de juillet se déroulera à distance, sur Meet. Seras tu disponible pour faire ce slot ?
 - [x] [[See why transfer-service is not returning internal accounts
 - [x] [[Bobelisation Virement]]
 - [x] [[funds-transfer]]
 - [x] add debtor/creditor name to funds_transfer request

## Investigate

Why do we get debtor accounts in both `debtor_accounts` and `creditor_accounts` endpoints ?

 ## Meetings
 
- [[CR! Instant Payment (IP)]]

## CRs:
 - 2nd July 2020
	 - [[CR! Bobelisation Virement]]

 - 19th June 2020 : 
	 - [[CR! Retour au site]]
	 - [[CR! Point Bugs Virement]]

 - 18th June 2020 :
	 - [[CR! Instant Payment (IP)]]