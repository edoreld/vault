# Rust

What is Rust?

## Characteristics

- Variables are immutable by default. Use `mut` after let to make a variable mutable.


```rust
io::stdin()
        .read_line(&mut guess)
```

