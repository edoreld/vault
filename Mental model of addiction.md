# Mental model of addiction
#addiction #model

Addiction is when you cannot stop your own harmful behavior despite knowing the consequences. 

## Causes of addiction

Addiction can be caused by things such as:

 - Depression
 - Unhappiness 
 - Lack of goals
 - Intolerance to boredom
 - Lack of self-awareness. See[[Rational vs Irrational MInd]]
 - Anger
 - Lack of emotional self-control

## Overcoming addiction

[[Meditation can help us overcome the lack of self-awareness]], [[Our environment can help us avoid falling into bad behaviors]]

