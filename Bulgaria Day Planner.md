# Bulgaria Day Planner

Eating: [[Where to eat in Bansko]]
Hiking Preparations: [[Hiking in Bansko]]

## Wednesday July 15

 - Arrive at 14:05
 - Either:
	 - Buy public transport ticket with Visa
	 - Get an odd amount of lev the airport (i.e: 40, 90) ATM or exchange bureau to pay for public transport. Don't choose to see transaction in EURO, as that will be charged extra.
 - Take metro 2 until Druzhba, then take bus 204 until bul. Bulgaria.
 - Go to sleeping place
 - Eat
 - Vitosha Boulevard
 - Walk around town
 - Exchange money at exchange bureau [here](https://goo.gl/maps/EBur477dvq6w7VpR7)
 - Sleep

## Thursday July 16

 - Breakfast/Coffee
 - National Museum of History (open) [10 leva] ( close at ~17:00)
 - Eat 
 - Boyana Church Museum (open) [10 leva] (close at ~ 17:00)
 - Eat

## Friday July 17

 - Travel to Bansko:
	 - Take a bus from Bus Station Ovcha Kupel 
		 -  7:00, 9:20 /except Tuesday/, 10:30, 11:50 /except Sunday/, 13:05, 13:55, 14:30, 14:40, 15:10, 15:28 /except Tuesday/, 15:55, 16:30, 17:00, 17:30. 
		 -  +359 2 955 5362
		 -  [ ] Call to verify
	 - OR Take a bus from Central Bus Station
		 -  7:30, 8.30, 9:45, 13:15 /except Sundays/, 14:00. 
 - Arrive in the afternoon
 - Check-in
 - Walk around old town

## Saturday July 18

 - Take the free tour at 11 am
	 - Location: In front of the “Holy Trinity Church”
	 - Duration: 1.5h-2h
 - Eat
- Visit  Bansko Tourist Information BTI [map] (https://www.google.com/maps/place/Bansko+Tourist+Information+(BTI)/@41.8289298,23.4815061,17z/data=!3m1!4b1!4m5!3m4!1s0x14abae9bf790446d:0xe17370886150046b!8m2!3d41.8289298!4d23.4836948)
- Visit “Paisii Hilendarski” Historical Center 
	- open 9am – 12 then 13:00 till 17:30


## Sunday July 19
 - Check Bansko's local market 
	 -  Fruits & Veggies [map](https://www.google.com/maps/place/41%C2%B050'18.7%22N+23%C2%B029'21.6%22E/@41.8384001,23.4873938,17z/data=!4m13!1m6!3m5!1s0x14abaea33e3f854f:0xe6c610e359acb05b!2sMuseum+%22Nikola+Vaptsarov%22!8m2!3d41.8376092!4d23.4888104!3m5!1s0x0:0x0!7e2!8m2!3d41.8385204!4d23.4893254)
	 -  Flea market [map](https://www.google.com/maps/place/41%C2%B050'05.9%22N+23%C2%B029'38.0%22E/@41.834959,23.4928007,18z/data=!4m9!1m2!2m1!1sstadium+bansko!3m5!1s0x0:0x0!7e2!8m2!3d41.8349585!4d23.4938954)
 	- Eat
 	- Buy supplies for hike

## Monday July 20
 - Visit the Permanent Icon Exhibition [map](https://goo.gl/maps/WC7b8r78qzrTbfCm6)
	- Monday to Friday from 9:00 AM – 12:00 PM and 2:00 PM – 5:00 PM
	- Free on this day
- Visit [[Nikola Vaptsarov Museum]] (anti-fascist poet)
	- Free on this day
	- 08:00 to 12:00, 14:00 to 18:00
- Visit the Holy Trinity Church [map](https://goo.gl/maps/iqQAtNzKNBPpEQQ88)
	- The complex is open for sightseeing every day from 8:00 AM to 6:00 PM.
- Visit [[Neofit Rilski]] House-Museum. 
	- 9:00 AM – 12:00 PM, 1:00 PM – 5:30 PM
- Visit [[Radonova house]] – 
	- every day from 9:00 to 12:00h and from 14:00 to 17:00h.

## Tuesday July 21

- Hike. Hikes saved in Wikiloc app. See [[Hiking in Bansko]]
- 
## Wednesday July 22
 - Start trip at 9:00
 -  Visit [[Stob's Pyramids]]
	 - Car park and hike for ~20 minutes
 - Lunch break
- Visit [[Rila Monastery]] & Monastery museum
- Back to Bansko 
- 
## Thursday July 23
 - Hike to Seven Rila Lakes. See Wikiloc app for ideas.

## Friday July 24
 - Hike to Dobrinishte and eat Ribarnika Restaurant and Trout Farm (catch your own fish potentially!)
 - Bathe at Hot Mineral Springs
	 - +359 88 730 3232

## Saturday July 25
 - Chill
 -  
## Sunday July 26

 - Leave for Sofia



## Ideas
- [[Hiking in Bansko]]:
	 - https://www.banskotouristinformation.com/summer/hiking
	 - ![[Bansko – Chalin valog – Eco path – Bansko.pdf]]
	 - ![[Bansko – Eco path – St. Nicola chapel – Dobrinishte.pdf]]
	 - ![[Bansko – Peshterite – Ancient fortress’ remains – Paligoden Monastery – Belizmata dam – Bansko.pdf]]
	 - ![[Bansko – Bunderishka poliana – Bunderitsa hut – Baikushevata mura (Baikushev’s white fir) – Okoto lake – Bansko.pdf]]

Renting a car
- Top Rent a Car Bansko
[[Trip to Bulgaria]]

 - Day trip Kavala, Greece (check duration on Google Maps)
	 - Check how long it takes to get there on the day
	 - Rent car? Public Transport ?
	 - Visit Ruins of Philippi on the way
	 - What to visit? [info](http://pop.sql-tutorial.com/kavala-greece-city/)

 - Gondola to go up. 
 	- [ ] Check if open (+359 749/88942)

 - Dancing Bears Park
	 - Come only on sunny days, not on rainy days
 - Rozhen monastery (2-hour trip)


