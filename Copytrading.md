# Copytrading
**<mark>Copytrading is a way to automatically copy the trades of another person</mark>**. 

You invest a certain amount of money into copying someone. Then, **every position that the copied trader opens triggers the opening of the same position in your own portfolio**. 

**A copied trade will be open with an amount proportional to the copied trader's position**. So for example, if you start a $1000 copy of a trader, and the copied trader opens a position that represents 1% of their portfolio, the same position with a value of 1% of the amount of your copy will be opened on your profile. In this case, it would be 1% of your copy of $1000, so $10. 

Why copytrade? The advantage is that it gives you **a way to invest in the market even with having no experience whatsoever**. You instead choose an expert to put your money to work for you.

One of the disadvantages is that you don't really know how the copied trader works, what her system is, so as soon as you start losing money with your copy you can be tempted to move to a different trader. 

