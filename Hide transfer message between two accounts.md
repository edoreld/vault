# Hide transfer message between two accounts

Lors d'un virement compte à compte du client (comptes dans son contrat), il ne doit pas y avoir de plafond de virement affiché sous le montant car il n'est pas limité dans les virements dans ses comptes.

Vérifier également que le plafond BEL n'est pas mis à jour suite au virement compte a compte

Si on dépasse la limite de solde + découvert alors la popin d'erreur s'affichera pour prévenir que le solde du compte est dépassé. (déjà traité dans la gestion des messages d'erreurs MPA)

 

Voici l'extrait des règles métier :
|*VIREMENT INTERNE (PERIMETRE CLIENT)*|§  Vers les comptes LCL dont le client est titulaire ou sur lequel il a une procuration :
 §  dans la limite du Solde de compte provisoire (SCP) et du découvert autorisé
 Le client peut faire un virement unitaire à la date du jour.|

 

Toutes les règles métier sur les virements sont dans le doc pour plus d'informations

 