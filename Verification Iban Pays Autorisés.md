# Verification Iban Pays Autorisés

US: https://jira-agile.fr.cly:8443/browse/RST-730

What to do:
 On the iban selection page, after selecting an iban, then confirming, there is an authorized country verification.
 
 First, we need to extract the 2-digit country code from the iban. Then, we need to check whether the 2-digit country code belongs in the list of authorized countries returned from the existing endpoint. 
 
 Working scenario:
  - Return 200

Not working scenario:
 - Coyu
 - Return Not Autorised

Verification to be done in two places:
 - When adding a beneficiary
 - After selecting an already existing beneficiary

 
 - [ ] When adding a beneficiary, what code is called? Add verification of 	country in this code
	 - Endpoint /iban/option
 

 - [x] When getting accounts to show in beneficiary list, don't return accounts whose iban is not in list of authorized countries.


## Thoughts

 - What already existing features exist that can be used for this US ?
	 1. Get authorized countries endpoint and associated service. What this does is **recover the list of authorized countries for one client** 
 - How to reuse my code so that I can cover both cases without duplication. 

