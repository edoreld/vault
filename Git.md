# Git

## Patch Mode

Patch mode, or `git add -p`, selectively adds portions of a file rather than the whole thing

**`git add -p` enters Patch Mode**. In this mode, Git shows you a hunk at a time - where **a hunk is an automatically detected block of code** - and asks you what you want to do with it. 

