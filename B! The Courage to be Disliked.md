# B! The Courage to be Disliked
#book

- Author:  Ichiro Kishimi, Fumitake Koga 
- Related to: [[Self-Determination]], [[Happiness]], [[Teleology]]

- ## Notes

We each have our own unique perception of the world which we don't share with anyone else and it depends on our subjective experience

Teleology the study of the purpose of a given phenomenon on rather than its causes. Studying the causes of a phenomenon is called etiology.

Teleology is, instead of searching in the past for the causes of our current behavior (etiology) - which leads to determinism, we ask what is the goal of the current behavior.

What purpose does the current behavior serve?

We are not determined by our experiences but its the interpretation we make of them to serve our purposes that is self-determining. 

We interpret things in a way that suits our purposes.

Everyone is living in the reality of their own making, and interpreting their experiences in a way that fits their goals. 

We are influenced by our emotions, but we have the final say on what we do. Our emotions are often a means to an end, rather than an inevitability.

I think Adler underestimates a little the power of emotions and impulses. It sounds like he really tries to deny that emotions have any effect on us at all and I just don't think that's right.

I wonder What part of these is Adler's psychology and what part is the cultural background of the author, a Japanese person who comes from a society where self control is highly valued.

So we see that we are not controlled by emotions or by our past. Emotions exist, but we are not controlled by them.

Adlerian psychology believes that when you're young you choose a certain lifestyle Which is based on unconscious factors such as your race your ethnicity your circumstances your parents etc. It also says this is something that you choose and so if you choose it when you're young you should be able to choose it again when you're an adult.

Adlerian psychology believes that at any time we can change our lifestyle, and the reason we tend not to do so is that we are accustomed to Our current lifestyle, It's like being a customs to use a certain computer, even if the computer is old slow and noisy, but it feels familiar, under decision to get a new computer takes courage because we are going to face a new situation where we don't know what to expect. In summary, we can change our lifestyle at any time but it takes the courage to face the unknown.

I think courage please are pretty important part in the decision to change her lifestyle, I don't think it's the only factor, but I think it's a good one

We make excuses for ourselves to not do the things that we say we want to do. We make statements such as "if only", so that we can keep living in this imaginary world where if only we had the time, my family we had the X factor, we would be able to do the thing that we're not doing, but in reality we're just using it as an excuse to not do that thing. Because we lack the courage to change, because actually taking a decision to make a change is fucking scary
