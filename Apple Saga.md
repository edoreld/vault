Apple sues Epic Games for breach of contract and seeks damages, says Epic poses “as a modern corporate Robin Hood” but just wants App Store's benefits for free [1]

[1]: https://www.cnbc.com/2020/09/08/apple-seeks-damages-from-epic-for-breach-of-contract.html


Previous posts about the Epic-Apple saga


 - https://etoro.tw/3bDpQEB Apple sues Epic
 - https://etoro.tw/3hG8lWn Apple bans Epic's App Store account
 - https://etoro.tw/3jmadUD Apple can't restrict use of Unreal Engine 
 - https://etoro.tw/2CTrnt1 Epic -- with support from $MSFT - says that $AAPL is threatening Unreal Engine
 - https://etoro.tw/2YrlcUP  - Apple reveals Epic CEO asked for special treatment   
 - https://etoro.tw/2EqX7Gz - Apple threatens to remove Epic's developer account
 - https://etoro.tw/2DYtoVd - Epic - 40% of which is owned by $0700.HK -- declares war against Apple