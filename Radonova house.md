# Radonova house 

The birth house of the adherent of Levski and Benkovski

One of the places that proves this fact is the Radonova house. This house is the first of its kind museum complex in Bansko. The expositions and the documentary materials in it directly testify to all the events and changes which the area has suffered during the Revival period. Anyone who is interested in the Bulgarian history shouldn’t miss to visit this memorable site in Bansko.

The Radonova house is a historical and ethnographic museum and its origins date back to 1971. 

urrently the Radonova house functions as the historical and ethnographic museum of the town of Bansko. Due to its rich history and collections, it has been declared a cultural monument of local importance. Thanks to its existence a small part of the rich past and lifestyle of the residents of Bansko is preserved.

In the Radonova house has lived Hadji Kandit Dagaradin, who was famous for being a prominent figure in the national liberation movement. It is known that he had been an adherent of Vasil Levski and Georgi Benkovski.

It is a well-known fact that the Radonova house has belonged to the rich family Hadjivulchevi and in particular to Mihail Dimitrov Hadjivulchev, who was born in 1854 and died in 1920.   He was a participant in the detachment of Bonyo Marinov. He left the house in order to go and live in Sofia. Mihail Hadjivulchev accommodated in the house his father-in-law Hadji Kandit Dagaradin.

Laten on Hadji Dagaradin was killed in this house. After his brutal death in the house was accommodated by Mihail Hadji Vulchev a relative of Hadji Kandit, who was Georgi Hadjiradonov – the father of Krum Radonov. Years later he bought the house and from that moment on it gets the name Radonova house.