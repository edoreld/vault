# Call

 - Revenues / Earnings growth ?
 - Quality Rating (S&P500)?
 - Debt to Current Asset Ratio : < 1.10
 - Current Ratio (current assets / current liabilities): > 1.50
	 - the more current assets the company has to fulfill its current liabilities
 - Positive earnings per share growth: + in last 5 years
 - Price to earnings per share ratio: 9.0 (eliminates high growth companies)

10-K
Earnings Report


 - Return on equity



Major drop prediction because:
 - large drops of GDP in many countries struck by the COVID pandemic
 - alarming unemployment rates
 - record breaking $GOLD  prices etc.),

Nevertheless, stock prices recently seem to be rather disconnected from the performance of national economies in favor of following their own growth/contraction cycle

As for the near future, my moves depend on the market development. If the situation does not change, I will probably not open many new trades, with certain exceptions, naturally
