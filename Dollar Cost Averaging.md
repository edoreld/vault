# Dollar Cost Averaging

DCA means putting a small amount of money into your copy every month, regardless of the price.

A great way to reduce risk, specially when you want to invest a large amount of money. Avoids peaks and bottoms ()

Why is this a good thing? When shares are more expensive, you'll buy fewer of them. When they're cheaper, you'll buy more of them. Overall, this will push down the average cost of your shares.

Once you decide to regularly invest smaller sums of money, it's important to make a commitment to a specific plan. If you just figure you'll invest whatever you have left at the end of each month, you'll probably find that "whatever" often translates to "nothing."

Then, the best way to make sure you follow through on your commitment is to move the money out of your bank account before you ever get a chance to spend it.
