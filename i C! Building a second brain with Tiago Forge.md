# C! Building a second brain with Tiago Forge
#course
- Author: Tiago Forte
- Related to: [[Productivity]], [[Organization]]



## Notes 

### Unit 1 Introduction
#### Sesssion 1 Part 1
 - Preface: [[V! Building a second brain with Tiago Forte | Building a second brain with Tiago Forte]]
 - The objective of the course is to learn a system of collecting, organizing and retrieving your ideas	using digital notes that maximizes the possibilities of creative breakthroughs.
 - *My* intention with this course is first and foremost to learn the unconscious mental models that guide my decisions. My second intention is to generate content for my blog and eventually for a Youtube channel.

#### Sesssion 1 Part 2
 - Tiago distinguishes 3 levels in note-taking: storing information, intermediate thinking and creative breakthroughs:
   - **Storing Information** is simply putting stuff into our note taking system
   - **Intermediate thinking** is when the note system is the basic for the creation of the art. It enables to break down big problems into small, doable ones.  
	   - > “The object isn't to make art, it's to be in that wonderful state which makes art inevitable.” - Robert Henri
   - **Creative Breakthroughs** is when the note taking system leads to new ideas, thoughts and connections.


- He shows a video of a F1 pit stop. In the video we see a pit stop from the past and one from the present. The one from the past takes more than 1 minute, while the one from the present takes a few seconds. He talks about how the combination of technology and humans aspects have enabled this improvement, and he compares it to [[Personal Knowledge Management]].
- He defines [[Personal Knowledge Management]] as the combination of the technology and human aspects in order to turn information into knowledge.
- He quotes research that says  that creativity is the result of **organizing information** and **collaborating** with others.

### Unit 2: Organizing for Creativity

#### Session 1 
- Are organizing and creative activity opposed concepts?
- What functionalities of a note-taking system are conducive to creative activity? Tiago distinguishes four:
	- **Promote links and associations between the different notes.**
		- > ".. increased sensitivity to **unusual associations** is another important contributor to creativity." - Harvard Business Review
		- It's an argument for keeping all your information in one place. 
	 - **Create visual artifacts**
	 - He gives the example of scientists who deal with abstract thoughts and yet build **physical models**. We think better visually than with abstract thoughts.
	 - He shows his Evernote notes where most notes have an **image** associated with them. 
	 - **Incubate ideas over long periods of time**
	 - Heavy lifting vs **Slow Burn** 
	 	- Heavy Lifting: tackling a project as something to do in one big session. He accepts that sometimes it might be necessary, but he says it's not sustainable.
		- Slow burn: slow, gradual, happening **in the background**. Keeping several projects open and trying to match new inputs to some of those projects.  
	- Reusing and Recycling
		- Using the notes that we've taken for a certain project and use that knowledge for future projects. 
	- **Provide the raw material for unique interpretations and perspectives**
		- **Information** has no intrisic **value. It's the interpretation and persuasion**** with which we present the information that gives it value.
		- The variety of ways in which we can display information (images, mindmaps, videos, etc) can lead to unique interpretations and arguments for persuasion.

####  Session 2

### Unit 3: Maximizing Return-on-Attention

####  Session 3
- **Return-on-attention** (ROA) is **the value we get out of paying attention**. Our note-taking system should maximize this value and deliver a positive ROA. 
 - He talks about [[Flow]]. 
- Any work has three different **preparation phases we need to go through before starting that work**;
	 - **Environment setup**: opening applications and websites, setting up the desk, etc.
	 - **Mental Setup:** what am I working on? 
	 - **Emotional Setup:** fighting the [[Resistance]]. 
 - **These phases consume our attention**, yet we need to go through them in order to reach the desired flow state.
 - When we deliver a functionality we are developing, all the work involved in getting to that functionality is lost, because we don't have a system of recycling and reusing the information. This can make us have to start future projects from zero instead of being able to rely on work we've done before. 
 - A possible remedy to this is <mark>**[[Placeholding]]: iteratively building value through smaller deliveries</mark> - "intermediate packets" or "modules" - which are packaged in a certain way that makes it possible to exploit them for future projects.**
 - Placeholding refers to placing "placeholders" along places in your projects, so you can come back to them later. 
 - **Examples of placeholding** would be the **notes, designs, illustrations**, charts, graphics, mindmaps, etc, that we iteratively build as we work on a project. 
 - There are at least three benefits for working this way:
	 - Harder to interrupt: no need to keep the whole project in your head, so interruptions' impact is lessened
	 - Easier to get feedback: it's easier for others to give feedback on small changes
	 - Able to work always on a task, no matter how little or how much time you have for that working session.
	 - Insights from previous projects can be used for new ones. Each project becomes a series of "lego pieces" which we can assemble in new ways to create different value.   [[Network Effect]] makes it so that new knowledge built on top of old one adds exponential value. 

- So what's the **relation between [[Flow]] and building projects as small packets of value**? If we take a look at some of the requirements for flow, we have:
	-  Instant Feedback, 
	-  Clear Goals
	-  Suitable Challenge-Skill Ratio [[i+1]]
 - And those are exactly the things enabled by working in small increments of value, such as with a note system. 
- The session finishes with a hint of next unit, by showing how the process of encoding and retrieving information from our intermediate packages of value must be intertwined with our work. 

### Unit 4: Progressive Summarization

 - notes > notebooks (think Evernote) > tags
 - What we care about is the notes themselves and their contents, now how they are organized. 
 - **Notes are all about Discoverability vs Understanding**:
	 - **Discoverability: compressing note** into metadata that allows us to get the meaning of the note quickly
	 - **Understanding: filling the note with plenty of examples and explanations** that make it easier to understand it.
 - <mark>Discoverability => Compression => Encoding</mark>
 - <mark>Understanding => Explanations => Decoding</mark>
 
 - **<mark>Progressive summarization is the process of iteratively summarizing (compressing) your note</mark>, over and over, until you get to a point where you can understand a note without needing to read the whole thing first**, thereby preventing loss of value due to needing to read a note before deciding if it's relevant. 
 - Tiago implements **progressive summarization though progressive layering**:
	 1. No filtering
	 2. Bold interesting parts
	 3. Highlight interesting-er parts
	 4. Mini-summary
	 5. Remix: creating your own content based on your interpreation of your notes (sort of like when creating illustrations for concepts in a bullet journal)
 - Primes the note for future you's understanding. 
 - One way to consume notes is to ask the question: how does this note help with my current problem?
 - One way to design notes is to think: how do I make as easy as possible to process these notes for future me? How do I make it as simple as possible to understand them for him, specially when he's tired, unmotivated, etc. 

### Unit 5: The Design of Discoverability 
















