	
	https://integration-transfer-transac.hors-prod.caas.lcl.gca

https://review-rst-730-re-54rh4c-transfer-transac.hors-prod.caas.lcl.gca

**Problème trouvé :**

On ne peut pas faire un virement avec un iban ajouté à la volée

**Cause :**

Juste avant de faire le virement, on vérifie que l'iban créditeur appartient à la liste de comptes externes de l'utilisateur. On fait ça pour éviter des virements vers d'ibans non autorisés. Parce que l'iban ajouté à la volée n'appartient pas à la liste de comptes externes de l'utilisateur, le virement échoue.

**Possibles Solutions**

Enlever la vérification de comptes externes
Detecter un compte de type volée (exploitable?)


Juste avant de faire le virement, on vérifie que l'iban créditeur appartient à la liste de comptes externes de l'utilisateur. On fait ça pour éviter des virements vers d'ibans non autorisés. Parce que l'iban ajouté à la volée n'appartient pas à la liste de comptes externes de l'utilisateur, le virement échoue.

Issues:
 - As John Tibi, I want add a German iban and transfer funds to it 
	 - Bug: transfer doesn't finish
	 - [ ] Research bug 
	 - [ ] 
 - [x] Test with WS02
 - [x] Submit PR and ask for one 👍🏽
 - [ ] Merge


Iban Examples:
 + France
	 - FR7630006000011234567890189
 - Germany
	 - DE75512108001245126199
 - Azerbaijan
	 - AZ96AZEJ00000000001234567890