# Bobelisation Virement

Info: [Confluence](https://confluence.fr.cly:8443/pages/viewpage.action?spaceKey=BIW&title=Application+WAR_BOBEL#ApplicationWAR_BOBEL-Bob%C3%A9lisationdesvirements)

La bobelisation consiste à **tracer dans une base les actions realisées par le client**

Afin de:** avoir un historique d'actions de l'utilisateur**

Le but de cette US est de mettre en place la bobelisation pour le virement d'argent.



## Infos Anne-Sophie

Bonjour,

Cette route ({{baseUrl}}/outil/WSBOBEL/bobel/virement) a été créée pour le virement entre amis (c’est un num tel bénéficiaire dans ce cas et non un IBAN).
Il n’existe pas de route pour les opérations de virement existantes (actuellement, la bobélisation se fait via une transaction mainframe qui appelle un service métier répondant au besoin)

According to Eric, the route for "Virement Occassionel" (QHVI) has already been developed by Nicolas. 

Les codes opérations BEL existant actuellement pour les virements sont les suivants :


**CODOPR**|**TXTOPR**
:-----:|:-----:
IQ32|Virement Europe format 320
QHVI|Virement occasionnel
UWMD|Modification virement différé
UWSD|Suppression virement différé
UWTI|Virement non SEPA
UWTS|Virement SEPA
UWVC|Création virement permanent
UWVD|Virement occas. différé
UWVM|Modification virement permanent
UWVS|Suppression virement permanent

Pour l’instant la refonte se concentre sur QHVI, UWTS.

Il faut prévoir une route côté WSBOBEL (pôle WEB) et une procstock (côté MVS) pour faire comme l’existant.

Cdt,

## How 

Requête
URL

webservice WSBOBEL_5

 

Exemple :
\<FQDN LISTE_WS>/outil/ WSBOBEL/bobel/virement

Methode HTTP

POST
Headers

**Clé**|**Valeur**
:-----:|:-----:
Content-Type|charset=UTF-8
Accept|application/json
authorization|vide
mode\_authent|Aucune
	
```json
{
    "client": {
        "idReper": "6581560543",
        "codeApplicatif": "CLI",
        "canal": "IN",
        "numServiceBel": "2",
        "numContractBCT": "8389"
    },
    "bobel": {
        "codeOperation": "VPLB",
        "topHistoBobel": "O",
        "topPubliBobel": "N"
    },
    "virement": {
        "codeDevise": "EUR",
        "codeBanqueDbt": 30002,
        "codeAgenceDbt": "00381",
        "numCompteDbt": "0000017030Z",
        "montant": 100,
        "motif": "motif du virement",
        "codeInc": "07616266364",
        "nomBeneficiaire": "Le Bénéficiaire"
    }
}
```

Le top histo et top publi c'est pour savoir si l'on historise et si l'on publie, les deux ne peuvent être à non en même temps

## Stocked Procedures



## Contacts
Nicolas Ribero








