# mock-api

This project can be used for mocking services which are interfaced with the front/back-for-front.

## Getting started

### Run mock-api server app

```shell
npm run dev:mock-api
```

### Run back-for-front with URLs pointing on mock-api

Configure URLs firstly:

```shell
# Unix
export ACCESS_TOKEN_SECRET_KEY=a2l6du49ro7zajf4g9wncvj40ksozj5f
export ACCESS_TOKEN_SECRET_IV=b00ne2i79kzjncns
export AUTHENTICATION_SERVICE_URL=http://localhost:3456/authentication
export CONTRACTS_SERVICE_URL=http://localhost:3456/contracts
export IDENTITY_PROVIDER_CLIENT_ID=idp_client_id
export IDENTITY_PROVIDER_CLIENT_SECRET=idp_client_secret
export IDENTITY_PROVIDER_URL=http://localhost:3456/identity-provider
export PRISMIC_TOKEN=MC5XNkpQaWlFQUFDQUFfbHg5.NO-_vVLvv73vv70rcu-_vUtQ77-9Yu-_ve-_vUzvv73vv71t77-977-9GEsB77-9Zizvv73vv70E77-977-977-9
export PRISMIC_WEBHOOK_SECRET=abc
export WSAFC_SERVICE_URL=http://localhost:3456/mfa-wsafc-service
export RISK_ASSESSMENT_SERVICE_URL=http://localhost:3456/risk-assessment-service
export USER_FRONT_SECRET_KEY=a2l6du49ro7zajf4g9wncvj40ksozj5f
export USER_FRONT_SECRET_IV=b00ne2i79kzjncns
export USER_ID_AT_INTERNET_ENCRYPT_KEY=AAMQz0bvQ72WcpRotODNSoM7ch51tlkoAR9Ub+ljQFA=
export USER_ID_AT_INTERNET_AUTHENT_KEY=g5aWs/c2b3w+PrPPljMNUyK7bhu42dDJHbfQa9NQk9U=
export USER_ID_DMP_ENCRYPT_KEY=AAMQz0bvQ72WcpRotODNSoM7ch51tlkoAR9Ub+ljQFA=
export USER_ID_DMP_AUTHENT_KEY=g5aWs/c2b3w+PrPPljMNUyK7bhu42dDJHbfQa9NQk9U=
export USER_SERVICE_URL=http://localhost:3456/user
export ACCOUNTS_SERVICE_URL=http://localhost:3456/accounts-service
export TRANSFER_SERVICE_URL=http://localhost:3456/transfer-service

# Window
set "ACCESS_TOKEN_SECRET_KEY=a2l6du49ro7zajf4g9wncvj40ksozj5f &
set "ACCESS_TOKEN_SECRET_IV=b00ne2i79kzjncns &
set "AUTHENTICATION_SERVICE_URL=http://localhost:3456/authentication &
set "CONTRACTS_SERVICE_URL=http://localhost:3456/contracts &
set "IDENTITY_PROVIDER_CLIENT_ID=idp_client_id &
set "IDENTITY_PROVIDER_CLIENT_SECRET=idp_client_secret &
set "IDENTITY_PROVIDER_URL=http://localhost:3456/identity-provider &
set "PRISMIC_TOKEN=MC5XNkpQaWlFQUFDQUFfbHg5.NO-_vVLvv73vv70rcu-_vUtQ77-9Yu-_ve-_vUzvv73vv71t77-977-9GEsB77-9Zizvv73vv70E77-977-977-9 &
set "PRISMIC_WEBHOOK_SECRET=abc &
set "WSAFC_SERVICE_URL=http://localhost:3456/mfa-wsafc-service &
set "RISK_ASSESSMENT_SERVICE_URL=http://localhost:3456/risk-assessment-service &
set "USER_FRONT_SECRET_KEY=a2l6du49ro7zajf4g9wncvj40ksozj5f &
set "USER_FRONT_SECRET_IV=b00ne2i79kzjncns &
set "USER_ID_AT_INTERNET_ENCRYPT_KEY=AAMQz0bvQ72WcpRotODNSoM7ch51tlkoAR9Ub+ljQFA= &
set "USER_ID_AT_INTERNET_AUTHENT_KEY=g5aWs/c2b3w+PrPPljMNUyK7bhu42dDJHbfQa9NQk9U= &
set "USER_ID_DMP_ENCRYPT_KEY=AAMQz0bvQ72WcpRotODNSoM7ch51tlkoAR9Ub+ljQFA= &
set "USER_ID_DMP_AUTHENT_KEY=g5aWs/c2b3w+PrPPljMNUyK7bhu42dDJHbfQa9NQk9U= &
set "USER_SERVICE_URL=http://localhost:3456/user &
set "ACCOUNTS_SERVICE_URL=http://localhost:3456/accounts-service &
set "TRANSFER_SERVICE_URL=http://localhost:3456/transfer-service"

```

Run back-for-front:

```shell
npm run dev:back
```
