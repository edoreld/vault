# Rila Monastery

Apart from being the most famous and most visited monastery in the whole country, Rila Monastery is the biggest holy site in Bulgaria, too.

It was found in the 10th century and is closely related to the life of Saint Ivan Rilski who moved to the place to live in seclusion.

The monastery was first built elsewhere but it was rebuilt on its actual place in 1335 by protosebastos Hrelyo.

During its long existence the monastery has always been a powerful literary and spiritual centre that protected and preserved Bulgarian identity. Prominent literary figures such as Paisiy Hilendarski and Neofit Rilski worked inside the monastery and Zahari Zograf himself participated in the painting of the church “Nativity” (1343).

The monastery offered protection and shelter to many Bulgarian revolutionaries such as Levski, Peyo Yavorov, and Gotse Delchev among others.

When you overstep the monastery threshold you are going to discover five-storey monastery building; Hrelyo’s Tower is going to impress you while “Nativity” church is going to fill you with humbleness.

How to reach Rila Monastery St. Ivan Rilski from Bansko?

The distance between Bansko and the monastery is 94 km and it is an hour drive. It is not complicated to reach the monastery, you just have to follow the road from Sofia to Kulata and take the turn-off towards Kocherinovo. There is a single road that will lead you directly to the monastery gate. Although the road has asphalt and is in a proper condition, it is narrow and full of turns so be careful.

Rila Monastery Saint Ivan Rilski is one of the One Hundred National Places of Interest and it is included in UNESCO heritage list, as well.