# identity-provider

Identity provider allows end users to authenticate and returns access_token, refresh_token, id_token etc.

It calls the authentication server to authenticate the end user, then generates a jwt which contains received authenticated information, and finally calls wso2-services which returns the tokens.

## JWT

A certificate containing public and private key is required at runtime to sign the jwt. For recette, the certificate is packed as a docker config in advance and imported into docker container when it is created (refer to docker-compose configs).

Current certificate is valid for 4 years.

JWT validation duration is **2 minutes**

## Create Truststore
Create Trust store:
```bash
cd certificates
./build-cert.sh
```

## Create secrets in Docker Swarm
Examples below are for `dvl` environment.

JWT Signing Key:
```bash
export ENVIRONMENT="dvl"
cat jwt-$ENVIRONMENT-signing-key.p12 | docker secret create -l "com.docker.ucp.access.label"="/Shared/Private/IACT-04015-UI" "SIT.$ENVIRONMENT.idp.jwt-signing-key.v1" -
```

Truststore:
```bash
export ENVIRONMENT="dvl"
cat idp-$ENVIRONMENT-truststore.jks | docker secret create -l "com.docker.ucp.access.label"="/Shared/Private/IACT-04015-UI" "SIT.$ENVIRONMENT.idp.truststore.v1" -
```

Application secrets:
```bash
export ENVIRONMENT="dvl"
cat application-secrets.yml | docker secret create -l "com.docker.ucp.access.label"="/Shared/Private/IACT-04015-UI" "SIT.$ENVIRONMENT.idp.application-secrets.v1" -
```
