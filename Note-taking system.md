# Note-taking system
#productivity #organisation

The value of the notetaking system comes from the association of notes to each other, as new concepts have a solid foundation from which to grow and be understood.

Notes taken in isolation and not linked to other concepts don't have a ground in which to grow and are prone to vanishing without a trace


## Backlinks
* [[Inbox]]
	* [[Note-taking system]]

