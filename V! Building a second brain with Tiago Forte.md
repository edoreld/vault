# V! Building a second brain with Tiago Forge
#video

Author: Tiago Forte
Source: https://www.youtube.com/watch?v=SjZSy8s2VEE
Related to: [[Productivity]], [[Organization]]

## Notes

 - Tiago says that the act of looking up information in different devices and applications incurs a cost, such as the time needed to search for that information as well as physical and metal fatigue.  He says that no matter how well-intentioned our organizing of information is, it will gradually become harder and harder to find, until it becomes lost completely. 
 - The amount of information we consume and process increases every year, but we continue to struggle to find the information we need. 
 - In Tiago's own experience, [[S! Knowledge Management | Knowledge Management]] is the biggest problem companies are facing today. Companies fail to implement top-down approaches to [[S! Knowledge Management | Knowledge Management]] because knowledge is ultimately personal; knowledge loses value when it's separated from the person that holds it.
 - That's why Tiago wanted to develop a knowledge management system that focuses on the person and on keeping the knowledge with the people that hold it. 
 - We spend a lot of time consuming new information and chasing new insights but we ignore the knowledge we already have. Tiago says that Personal [[S! Knowledge Management | Knowledge Management]] is a way of exploiting our existing knowledge. 
 - The "Building a Second Brain" course created by Tiago teaches people to create and manage a personal [[S! Knowledge Management]] system. 
 - The benefits of building a second brain are:  
	 1. [[V! Building a second brain with Tiago Forte#Offloading your memory into an external, persistent storage system | Offloading your memory into an external, persistent storage system  ]]
	 3. [[V! Building a second brain with Tiago Forte#Interconnecting Ideas | Interconnecting Ideas]]
	 4. [[V! Building a second brain with Tiago Forte#Sharing yor mental models with others | Sharing yor mental models with others]]

### Offloading your memory into an external, persistent storage system
 - Your second brain is the reposity where you store the information that comes from all your apps and devices.  

### Interconnecting Ideas
 - Information links to other information in the form of notes linking to other notes. This encourages new insights. 

### Sharing yor mental models with others
 - Sharing information about a certain topic becomes a question of finding the information about the topic in our second brain and making it available for others. 


## References:
 - [Building a Second Brain: Capturing, Organizing, and Sharing Knowledge Using Digital Notes](https://www.youtube.com/watch?v=SjZSy8s2VEE)

