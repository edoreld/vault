# Authentication Service

## Overview

This microservice calls:
- the transaction `T5W1` to authenticate the client by access code.
- the transaction `T5A2` for multi-factor authentication which require that users prove their identity with more than one type for increased security.
- the transaction `T5MO` to update client access code.
- the transaction `T5AF` to log user multi-factor authentication.

## Create Truststore
MongoDB certificate can be extracted from an existing `mongo.ks` trust store.
This trust store can be found in [Alfred](https://alfred.lcl-ul.com/com.cl.outil2.wsgestop/).

Create mongo key from mongo.ks:
```bash
cd certificates
./build-mongo-cert.sh ../path/to/mongo.ks mongodb
```
Generate truststore:
```bash
cd certificates
./build-truststore.sh
```

## WSAFC client certificate
For Multi Factor Authentication unenrollment (WSAFC API), .p12 keystore files are used for client authentication.
These files come from [Alfred](https://alfred.lcl-ul.com/com.cl.outil2.msafcbel/): `LCL_AFC.p12` (dvl) or `LCL_AFC_PROD.p12` (plt / prd)

To change the password of .p12:
```bash
cd certificates
./change-wsafc-keystore-password.sh dvl LCL_AFC.p12
./change-wsafc-keystore-password.sh plt LCL_AFC_PROD.p12
./change-wsafc-keystore-password.sh prd LCL_AFC_PROD.p12
```

The truststore and WSAFC client certificate previously generated  must be encoded in base 64 to be able storing them on Vault  
To encode the generated truststore:
```bash
base64 auth-dvl-truststore.jks -w0 > encoded-truststore.jks`
```
```bash
base64 LCL_AFC.p12 -w0 > ENCODED_LCL_AFC.p12`
```

Store the encoded files in the right namespace and path in Vault.
The path must be the same one used in kustomize manifests.
