# Point Compagnon

## Débrief de la réunion avec Prismic

Utiliser prismic pour recuperer les messages et les afficher dans un format specifique

titre - texte - lien

avoir plusieurs messages en sequence

playlist pour afficher des messages en sequence

messages ephemeres qui s'affichent qu'une seule fois

release: une fois publiée on peut programmer la dépublication

Pour le moment on enregistre pas si un utilisateur a vu un message

**Trigramme**: 
- i.e. "ta nouvelle carte est arrivée" -> personalisé par utilisateur

Chercher les trigrammes (base de données BEL) qui correspond à un utilisateur et chercher en Prismic le message. Notion de priorité. Quel message afficher en priorité ?

Dans prismic un champ (i.e: priorité) 

Chercher trigramme dans prismic et priorité dans BEL

champs UID 

**Variables**

Bonjour, monsieur {{nom}}

Variables pas possible dans Prismic pour le moment.

Comment gérer la limitation de nombre de caractères vim

**Emojis**

Working except on IE


**Questions**
 - A way to close notification? "No"

Bannière en dur en haut dans la page principale

Contenu du compagnon alimenté dans Prismic

Accessibilité ? Pas de carroussel prévu. Clique sur un message du compagnon fait apparaitre le prochain tout de suite. Afficher les messages par priorités. 

Pas de date d'expiration sur le message ? Pas de façon de fermer le message ? Campagne avec date de début et de fin ?

Webservice pour récupérer les messages de Prismic et base d'alertes BEL

Compagnon personnalisé (alerte BEL)

Base alertes BEL

Alertes délivrées avec le compagnon. Alertes avec le trigramme comme clé.

Historique pour les alertes utilisateur ?

Base alertes + messages génériques **en priorité**
