# Développer un endpoint dans transfer-service pour executer le virement

Tracked in: [Vérifier appel à la BCT](https://jira-agile.fr.cly:8443/browse/RST-728)
Related to: [[Virement]]
Affects: [[Transfer-Service]]
Swagger: [link](https://confluence.fr.cly:8443/download/attachments/78807244/swagger_srv_funds_V2.1.yaml?version=1&modificationDate=1591796232000&api=v2)

## Clipboard

```java
    public boolean debtorIbanPresentInDebtorAccounts(String iban, List<InternalAccount> debtorAccounts) {
        return debtorAccounts.stream().anyMatch(internalAccount -> iban.equals(internalAccount.getIban()));
    }

    public boolean creditorIbanPresentInCreditorAccounts(String iban, CreditorAccountsResponse creditorAccountsResponse) {
        return creditorAccountsResponse.getInternalAccounts().stream().anyMatch(internalAccount -> iban.equals(internalAccount.getIban())) ||
                creditorAccountsResponse.getExternalAccounts().stream().anyMatch(externalAccount -> iban.equals(externalAccount.getIban()));
    }

```


## TODO
 - [ ] Add Integration test
 - [x] Improve/Clean up FundsTransferProviderTest
 - [ ] Check the validity of all data (debtor, creditor, amount) before calling MPA.
 - [ ] 
## Test cases

John TIBI
1850276567 / 00 09 74
Contrat 0031302305553640  KO

## Question
 - How to authorize this request ? Is it by JWT token just like the funds_transfer_options ?
	 - Authorization though user and pass (see [[Développer un endpoint dans transfer-service pour executer le virement#Swagger | below]] )

## Notes
 - The team that manages the endpoint is the same that handles /funds_transfer_options, so Sarra et al. 

## Tidbits

* N'existe-t-il pas un champ pour renseigner la référence du virement ? (motif = "transfer_informations.remittance", référence = ?) Si tu parles de la EndToEnd, pour l’instant nous n’avons effectivement pas prévu ce champ pour les particuliers

# Body of request from BFF

POST /funds_transfer

```json
{
    "contract_id": "123213213213",
    "debtor": {
        "iban": "FR321432"
    },
    "creditor": {
        "iban": "FR123213"
    },
    "information": {
	    "is_instant_payment": true,
        "amount": 100,
        "remittance": "remittance",
        "reference": "reference"
    }
}
```

# HTTP Request & Response
How to generate each of the fields in the request:

 - request_date: generate it
 - usage: given
 - acquisition_chanel: given
 - user_profile_code: given
 - transfer_type: given
 - originator_name: pressumably, name of originator
 - originator_id: id_reper
 - debtor {}: same as the one in funds_transfer_options
 - creditor {}: same as the one in funds_transfer_options]
 - transfer_informations {}:
	 - amount: amount
	 - remittance: motif du virement
	 - endtoend_identification: not for particuliers
	 - creditor_account_bearer: Virement vers un compte de son périmètre explicite de titularité
 - risk_analysis_data: ??? 

Body of the request:

```json
{
  "request_date": "2020-06-17T09:04:27.534Z", 
  "usage": "IBN",
  "acquisition_chanel": "IN",
  "user_profile_code": "L",
  "transfer_type": "VIO",
  "originator_name": "string",
  "originator_id": "string",
  "debtor": {
    "name": "string",
    "identifier": "string",
    "account_identification": {
      "iban": "string",
      "bic": "string",
      "alias": "string"
    }
  },
  "creditor": {
    "name": "string",
    "identifier": "string",
    "account_identification": {
      "iban": "string",
      "bic": "string",
      "alias": "string"
    }
  },,
  "transfer_informations": {
    "amount": 0,
    "remittance": "string",
    "endtoend_identification": "string",
    "creditor_account_bearer": true
  },
  "risk_analysis_data": {
    "customer_application": "LCLMC (LCLMesComptes), CLI, CLA, ...",
    "ceiling": "Customer ceiling for the current transaction",
    "recipient_alias": "only for Paylib usage",
    "partner_id": "originator partner id",
    "login_id": "only if customer_application is CLI, CLA, CLE",
    "strong_authentication": "strong authentification level",
    "imei": "only if customer_application is LMCP",
    "ip_address": "only if customer_application is CLI, CLA, CLE",
    "User_agent": "Browser",
    "edi_requestor": "for DSP2"
  }
}
```

## Swagger

**Authentication**:

```
https://rct-wsctxd01.lclhp-ee.docker.prodinfo.gca/WSCTXD01/funds_transfer
2JqrSh2sd4xw5SdkXdWh4HrKU9XvuF6s
6wZaGmvtRTJbq9xWujRWSNtBKvgz345Z
```

```json

swagger: '2.0'
info:
  version: "1.0.0-SNAPSHOT"
  title: CreditTransferAPI
  description: ''
schemes:
  - https
host : "rct-wsctxd01.lclhp-ee.docker.prodinfo.gca"
basePath: "/WSCTXD01"
securityDefinitions:
  basicAuth:
    type: basic

security:
  - basicAuth: []
paths:
  /funds_transfer:
    post:
      tags:
        - Funds transfer
      summary: Triggers a fund transfer based on the provided options
      description: >
        If the fund transfer is based on a phone number, this operation retrieves the IBAN based on PayLib's API,
        then if an instant payment is possible it will be triggered, otherwise a classic SCT will be made.
        If the fund transfer is based on the IBAN, the type of transfer must be provided.
      parameters:
        - in: body
          name: body
          description: .
          required: true
          schema:
            $ref: '#/definitions/FundsTransferRequest'
      produces:
        - application/json
      responses:
        '200':
          description: The transfer has been executed correctly.
          schema:
            $ref: '#/definitions/FundsTransferResponse'
        '400':
          description: >-
            Bad request format or missing mandatory fields (GAFI errors). The
            only GAFI errors leading to an execution report (CRE) generation
            are  missing debto/creditor address infos.
          schema:
            $ref: '#/definitions/ErrorResponse'
        '403':
          description: >-
            Error due to inconsistancies in the request, regulatory reasons or specific transaction rules.
          schema:
            $ref: '#/definitions/ErrorResponse'
        '500':
          description: >-
            Internal server error occurred while processing the SCT Inst
            submission.
          schema:
            $ref: '#/definitions/ErrorResponse'
  /funds_transfer/resume:
    post:
      tags:
        - Funds transfer
      summary: Triggers a fund transfer based on the provided options
      description: >
        If the fund transfer is based on a phone number, this operation retrieves the IBAN based on PayLib's API,
        then if an instant payment is possible it will be triggered, otherwise a classic SCT will be made.
        If the fund transfer is based on the IBAN, the type of transfer must be provided.
      parameters:
        - in: body
          name: body
          description: .
          required: true
          schema:
            $ref: '#/definitions/ResumeFundsTransferRequest'
      produces:
        - application/json
      responses:
        '200':
          description: The transfer has been executed correctly.
          schema:
            $ref: '#/definitions/FundsTransferResponse'
        '400':
          description: >-
            Bad request format or missing mandatory fields.
          schema:
            $ref: '#/definitions/ErrorResponse'
        '403':
          description: >-
            Error due to inconsistancies in the request, or unauthorised credit transfer.
          schema:
            $ref: '#/definitions/ErrorResponse'
        '422':
          description: Unprocessable request due to inconcistancies. For example the creditor's BIC not matching his IBAN
          schema:
            $ref: '#/definitions/ErrorResponse'
        '500':
          description: >-
            Internal server error occurred while processing the SCT Inst
            submission.
          schema:
            $ref: '#/definitions/ErrorResponse'
  /funds_transfer/timeout:
    post:
      tags:
        - Funds transfer
      summary: Triggers a fund transfer cancellation
      description: >
        If the fund transfer is based on a phone number, this operation retrieves the IBAN based on PayLib's API,
        then if an instant payment is possible it will be triggered, otherwise a classic SCT will be made.
        If the fund transfer is based on the IBAN, the type of transfer must be provided.
      parameters:
        - in: body
          name: body
          description: .
          required: true
          schema:
            $ref: '#/definitions/TimeoutFundsTransferRequest'
      produces:
        - application/json
      responses:
        '200':
          description: The cancellation has been executed correctly.
          schema:
            $ref: '#/definitions/FundsTransferResponse'
        '400':
          description: >-
            Bad request format or missing mandatory fields.
          schema:
            $ref: '#/definitions/ErrorResponse'
        '403':
          description: >-
            Error due to inconsistancies in the request, or unauthorised credit transfer.
          schema:
            $ref: '#/definitions/ErrorResponse'
        '422':
          description: Unprocessable request due to inconcistancies. For example the creditor's BIC not matching his IBAN
          schema:
            $ref: '#/definitions/ErrorResponse'
        '500':
          description: >-
            Internal server error occurred while processing the SCT Inst
            submission.
          schema:
            $ref: '#/definitions/ErrorResponse'
definitions:
  FundsTransferRequest:
    type: object
    description: FundsTransferRequest's scheme
    required:
      - request_date
      - usage
      - acquisition_chanel
      - user_profile_code
      - originator_name
      - originator_id
      - debtor
      - creditor
      - transfer_informations
    properties:
      request_date:
        type: string
        format: date-time
        description: 'ISOdate = YYYY-MM-DDThh:mm:ss.ffffff'
      usage:
        type: string
        maxLength: 3
        description: PLB / IBN
      acquisition_chanel:
        type: string
        maxLength: 2
        description: Internet, Mobile (MO), Intranet, LSB ...
      user_profile_code:
        type: string
        maxLength: 2
        description: Internet user (L), employee (C)...
      transfer_type:
        type: string
        maxLength: 3
      originator_name:
        type: string
        description: Name used for messaging purposes. (Creditor notifications for instance)
        maxLength: 70
      originator_id:
        type: string
        description: NUMTECPRS if it is a LCL debtor
        maxLength: 10
      debtor:
        $ref: '#/definitions/Party'
      creditor:
        $ref: '#/definitions/Party'
      transfer_informations:
        $ref: '#/definitions/CreditTransferInformations'
      risk_analysis_data:
        type: object
        additionalProperties:
          type: string
        example:
          customer_application : LCLMC (LCLMesComptes), CLI, CLA, ...
          ceiling : Customer ceiling for the current transaction 
          recipient_alias: only for Paylib usage
          partner_id : originator partner id
          login_id : only if customer_application is CLI, CLA, CLE
          strong_authentication : stong authentification level
          imei : only if customer_application is LMCP
          ip_address  : only if customer_application is CLI, CLA, CLE
          User_agent : Browser
          edi_requestor : for DSP2
  FundsTransferResponse:
    type: object
    description: TransactionResponse's scheme
    required:
      - pending_iban_search
    properties:
      pending_iban_search:
        type: boolean
      time_limit:
        type: integer
        minimum: 0
        maximum: 99
  ResumeFundsTransferRequest:
    type: object
    required:
      - code
      - iban_search_ref
      - original_request
    properties:
      iban:
        type: string
        minLength: 1
        maxLength: 34
      code:
        type: string
        description: Paylib RspnCode. 00-0000 if OK
        minLength: 7
        maxLength: 7
      iban_search_ref:
        type: string
        minLength: 10
        maxLength: 10
      original_request:
        type: string
        minLength: 0
        maxLength: 4096
  TimeoutFundsTransferRequest:
    type: object
    required:
      - code
      - iban_search_ref
      - original_request
    properties:
      code:
        type: string
        description: Paylib RspnCode. 00-0000 if OK
        minLength: 7
        maxLength: 7
      iban_search_ref:
        type: string
        minLength: 10
        maxLength: 10
      original_request:
        type: string
        minLength: 0
        maxLength: 4096
  ErrorResponse:
    type: object
    description: TransactionResponse's scheme
    properties:
      code:
        type: string
      message:
        type: string
  Party:
    description: Party's information
    type: object
    properties:
      name:
        description: Name of the party
        type: string
        minLength: 1
        maxLength: 70
      identifier:
        type: string
        description: NUMTECPRS if it is a LCL debtor
      account_identification:
        $ref: '#/definitions/AccountIdentification'
  AccountIdentification:
    description: Transaction information
    type: object
    properties:
      iban:
        type: string
        minLength: 1
        maxLength: 34
        description: When funds_transfer,must be present
      bic:
        type: string
        minLength: 1
        maxLength: 11
      alias:
        type: string
        maxLength: 60
        description: mail or phone number registered at PayLib's. Ignored if Debtor
  CreditTransferInformations:
    required:
      - amount
    description: Transaction information
    type: object
    properties:
      amount:
        description: >-
          The amount in Euro. If present, must numeric
        type: number
        format: double
      remittance:
        type: string
        minLength: 1
        maxLength: 140
      endtoend_identification:
        description: If present, must not begin with a space. Only one space betwenn words is allowed 
        type : string
        minLength: 1
        maxLength: 35
      creditor_account_bearer:
        type: boolean
        description: false if not present
```