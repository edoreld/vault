# funds-transfer

USs : 
 - [[Développer un endpoint dans transfer-service pour executer le virement]]

## Context

{"headers":{"Accept":["application/json"]},"body":null,"method":"GET","url":"https://rct-comptes-datalayer.lclhp-ee.docker.prodinfo.gca/notoken/comptes?idReper=NzUxNDIyNDUxMA&idBct=MDAzMTMwMzUwODA2NDcyMw","type":null}

## TODO
  - ask about reference_id
  - ask about correspondent_label
  - Assign correspondent_label to name in request to funds_transfer
 - remove funds_transfer_options call when funds_tranferring and reuse accounts_service call (6.33 seconds to 4.03 seconds)
 - encode contract_id + encode debtor_iban + encode creditor_iban