# eToro Blog Posts

Pinned post: https://etoro.tw/36JWBPU

Post reassuing investors: https://etoro.tw/2GwxENk

### INTEL

If $INTC is not the most important strategic asset in the US right now, it’s up there. 

Intel has lost its way with the wrong leadership and the wrong focus. The engineering roots were placed on a lower pedestal to profits, which is a common mistake, but one that I think may have to be rectified for national security purposes which could also unlock significant value for the stock.

~~ Quick Background ~~

While Intel has a handful of different, interrelated businesses, from a high level we can kind of think of Intel’s business as two parts: chip design and chip manufacturing.

Typically in the business of fabs (microchip manufacturing plants), you invest billions building new manufacturing capacity for a new generation of chips. This requires a large upfront investment that depreciates rapidly and provides a substantial margin in the out years as the deprecation falls off.

In 2016, instead of investing in the next-gen of chip manufacturing capacity, $INTC decided to double down on 14nm fab capacity. We can interpret this as management thinking that the opportunity there was large, but also that the manufacturing team was potentially falling behind the design team. Since that point in time, it has continued to miss targets for the fab business on next-gen chips.

This his has been allowed to happen (and the stock hasn’t been punished too bad over this time frame being up ~50% since the beginning of 2016) because profitability has continued to expand due to the shift to the cloud and the dominant position of $INTC in server chips:

>> “This is a company that has completely lost its way technologically for years now, but because of its dominant position in a category (server chips) driven by secular changes in computing (cloud shift in back-end services plus mobile-driven explosion in usage) it has yet to feel any sort of financial pain from that failure; this, sadly, has only led to further failures, and as I noted last month, serious national security vulnerabilities for the United States. This is abject management failure.”  -Ben Thompson, Stratechery

~~ The Opportunity ~~

As chips move to 7nm, Intel’s fabrication business is at least 12-24 months behind internal targets, giving an edge to TSMC and $SMSN.L . As it falls further behind the technology curve, the pressure is building to use external workshops for manufacturing (most likely TSMC and possibly Samsung). Here’s what CEO Bob Swan said on the earnings call:

>> “We’ve root-caused the issue and believe there are no fundamental roadblocks, but we have also invested in contingency plans to hedge against further schedule uncertainty…..We will continue to invest in our future process technology roadmap, but we will be pragmatic and objective in deploying the process technology that delivers the most predictability and performance for our customers, whether that be in our process, external foundry process, or a combination of both.” (emphasis mine)

This sounds like a precursor to Intel becoming a fabless chip designer. However, having Intel’s chip manufacturing business endure a full demise presents a national security risk, ensuring most chips are manufactured abroad.

The momentum is already building, quickly, that something has to be done to ensure that the US has significant semiconductor fabrication capacity. Accepting that chips are fabricated in Taiwan, South Korea, and Japan is no longer acceptable.

As a side note, let’s think about what management is seeing over at $AMD. Since 2016, AMD’s stock price is up ~2800% vs. ~50% for INTC. AMD had previously spun off its chip manufacturing business into GlobalFoundries and it's using GlobalFoundries, TSMC, and Samsung to manufacture chips. AMD CTO has said he doesn’t regret going fabless as it has allowed them to focus on chip design without worrying about manufacturing and the bottleneck it creates. I'm sure he does not regret the nice increase in the value of his shares/options even more.

### 

Hi all,

We've gone through a few months of fantastic growth, with April and August by far the best months ever in the portfolio's history. Wouldn't it be nice if the portfolio kept going 14% up every month?

However, as you may have noticed, the market didn't do so well in September. You should know that it's normal. The market regularly goes through down periods (called pullbacks), specially after growing rapidly. 

Over the long-term, stocks are still one of the best places you can put our money in. As long as you can stay invested through the bad and good times, you can build your wealth and be successful. 

By investing only the money you don't need, staying calm during down periods and re-investing regularly, you can definitely be successful. 

I invite you to join me and my copiers in building our wealth. 77.27% of copiers of this portfolio are profitable and the average duration of copy is two months. 	

>> "How many millionaires do you know who have become wealthy by investing in savings accounts? I rest my case." - Robert G. Allen

With all that said, thank you to all the long-term and the many new copiers.

And here's some articles I've written recently that you might find interesting:

$APH https://etoro.tw/30qRoIU
$SNOW https://etoro.tw/3jnpbKl
$DJ30 $SPX500 $NSDQ100 Thoughs on elections https://etoro.tw/34tlqwA

Best,
Javier





Should you buy $APH ?

Amphenol is a leading manufacturer of electronic and fiber optic connectors, antennas, and sensors with ~$8bn in sales. It is a huge (~$175bn), fragmented, and growing market that has benefited from the spread of electronics into just about everything we do.

The company has enjoyed steady growth over the past 2+ decades by getting involved in many different sectors. This helps shield it from the wild ups and downs of the market. Since 2003, sales have increased 13% annually with just one down year.

While results have softened since mid-2019 on account of the trade war with China and now the challenges related to COVID-19, Amphenol is well-positioned to benefit from a cyclical rebound in volumes in 2021/22.

The total market for interconnect and sensor products supplied by APH is estimated at ~$175bn, suggesting the company has a 4-5% market share. The company is broadly diversified, with no end market larger than 20% of sales, and has a leading market share in aerospace and defense, is the #3 competitor in Auto, and is #2 in Industrial. The company also enjoys very strong market positions in mobile devices, networks, and IT, and Data Communications, having invented the standards applications.

The company trie to be everywhere as new technologies are deployed and incremental operations are made efficient, and diversification allows the company to always ‘be’ where the innovation is most obvious. This has resulted in a balanced mix across eight different end markets each of which is <20% of sales.

Last year was an surprisingly tough year for Amphenol as the company was caught in the cross current of trade headlines which negatively impacted several end markets. The company was negatively impacted by trade issues related to Huawei which reverberated throughout the tech supply chain and negatively impacted demand in IT and Datacom (19% of sales) and Mobile Networks (8%). It also was facing a tough prior year in the Wireless end market (13%). And at the same time, weaker than expected demand in worldwide auto (19%) and industrial (20%) end markets were exacerbated by distributor inventory reductions. As a result, overall organic sales fell 3% on the year. In 2020, I expect further declines in organic sales related to Covid-19 challenges in most of its end markets.

Risk Factors:

·        Exposure to China/Global Trade. APH generates 73% of sales outside the U.S. and ~32% of sales in China. A large part of its sales in China are to suppliers of electronics that make products for the rest of the world. APH’s decentralized model allows it to operate locally in all the markets/regions it operates in, and the company generally attempts to match up production with demand in given countries. This lessens its exposure to tariffs and other trade disputes but does not eliminate the risks to the extent that trade relations continue to get worse.

·        Further economic weakness. APH operates in cyclical businesses. It can offset the effect of cyclicality through diversification, organic growth/market share gains, and mergers & acquisitions, but is not fully immune to the economy. To the extent that the current recession deepens and/or recovery does not take hold in 2021, APH results would be negatively impacted.

·        Poor execution of organic or inorganic growth strategy. New product introductions, which Amphenol defines as products introduced in the past 2 years, are very important to driving sales growth and maintaining strong profitability, accounting for ~25% of revenues. To the extent the company misses out on the latest trends in the electronics revolution or fails to buy the right companies in growing verticals, its sales growth may disappoint.

·        Key man risk. Amphenol is lead by the very capable Adam Norwitt. He has been in charge since 2009 but is still very young (51) and has $300+mn reasons to want to continue leading the company. To the extent he was to leave to chase after another opportunity without grooming a successor, the leadership void would likely weigh on its premium valuation until the successor demonstrated as effective a communications and capital allocation acumen. 


## To Plagiarize

IF YOU ARE NOT PROFITABLE, HERE ARE 7 THINGS YOU MIGHT BE DOING WRONG:

1. You do not have a clear investment strategy that actually works.
2. You focus on low-quality stocks. Or you try to guess/pick jackpot stock.
3. You do not feel hungry to get more. Constantly feeling complacent.
4. You do not try to learn from other people's experiences or even mistakes.
5. You do not have a mentor or teacher.
6. You believe in luck rather than consistency.
7. You are using such phrases in your head as "It's not my fault"; "It's not fair"; "I do not have time to study / analyze that"... and many more that are not helping you.

Enjoyed this post? Leave a like!
Do not agree with this? Leave a comment and let's discuss!
Add me to your watchlist, if you want to see similar content.

Have a green day!

$TSLA (Tesla Motors, Inc.) $AAPL (Apple) $BTC $SPX500 $NIO (Nio Inc.




The U.S. election is coming up. How will this affect the $SPX500  , the $DJ30  , and the rest of the stock market? 

While nobody has a crystal ball to tell the future, we can look at previous elections to see what's happened before. 

First of all, we need to be ready for increased volatility. I know this can make some feel uneasy. I get it. However, it's at this time when more than ever we need to put our emotions aside and objectively asses the situation. 

Don't invest based on predictions. Remember 2016? Everyone thought Hillary had it in the bag, but when states started announcing the results about a Trump victory, the $SPX500 fell more than 5% in premarket trading. So yeah, be careful of predictions. 

Stocks have gone up and down during elections, but on the long-term the trajectory is always positive. Instead of focusing on the headlines, the best we can do is focus on making our portfolio better.

Have a good day,
Javier



Expect turbulence today as the 

Lovely discounts all across the board today

>> Global stocks slid on Thursday, while the dollar edged up, after the US Federal Reserve gave no indication that any new stimulus measures would be forthcoming, as the economy continues to recover from the effects of the coronavirus pandemic. [1]



[1]: https://www.businessinsider.fr/us/stock-markets-futures-drop-dollar-gains-after-fed-outlook-2020-9


Here's all that you missed from $AAPL 's  "Time Flies" events from yesterday:

>> Apple debuts Apple Watch Series 6 for $399 in new colors, including red and graphite, with a similar design, a chip that's up to 20% faster, blood oxygen sensor [1]

>> Apple One bundles launch with discounts on services including Music, TV+, Arcade, iCloud storage, Fitness+, and News+ and tiers between $15 and $30/month [2]

>> Apple updates iPad Air with 10.9" edge-to-edge display, iPad Pro-like design, Touch ID in power button, USB-C, and A14 chip, shipping next month from $599 [3]

As an Apple investor, I really enjoy seeing them push further into services. The new Watch and iPads look great too. 


[1]: https://www.theverge.com/2020/9/15/21431365/apple-watch-series-6-price-features-specs-release-date?scrolla=5eb6d68b7fedc32c19ef33b4
[2]: https://www.theverge.com/2020/9/15/21433205/apple-one-subscription-bundle-price-music-tv-plus-arcade-icloud?scrolla=5eb6d68b7fedc32c19ef33b4
[3]: https://www.theverge.com/2020/9/15/21436500/apple-ipad-air-new-design-features-colors-release-date-price?scrolla=5eb6d68b7fedc32c19ef33b4




[[Apple Saga]]

Copiers and followers,

I've just finished adding funds to my account as per my previous post. 

If you were considering copying, but didn't want to do it before I added funds, you can do so again.

If you are a copier already, and wondering what you should do, please take a look at this post: https://etoro.tw/33xHUwi

Have a fine day,
Javier

What to say if I add funds: https://etoro.tw/3bj2Zy0

Guidelines: ![[Guidelines for blog posts by Popular Investor(1).pdf]]




# eToro Posts

$AAPL making a new phone and watch

>> Apple officially announces a virtual event for September 15, where it's expected to unveil an all-new lineup of iPhone 12 models and an Apple Watch Series 6 [1]

The Watch's oxygen level detection sounds like a cool feature for runners and other athletes. 

[1]: https://9to5mac.com/2020/09/08/apple-iphone-12-event-september/




 Sources: Xbox Series X will be priced at $499 and will launch on November 10, 2020 alongside the Xbox Series S  —  The Xbox Series S and Xbox Series X together.  —  Microsoft is set to hold a press event soon to showcase the next-gen console pricing, and it's pretty familiar.  —  What you need to know

[link](https://etoro.tw/2DYtoVd)
## Epic declares war against Apple

Yesterday, Epic announced a new update for to Fornite for iOS:

>> Today, we’re also introducing a new way to pay on iOS and Android: Epic direct payment. When you choose to use Epic direct payments, you save up to 20% as Epic passes along payment processing savings to you.

Epic shipped this update directly through the Fornite app, not through a new version of the app delivered through the App Store. And yes, that's against the rules. 

Apple could have chosen to block Fortnite updates until Epic removed the new payment feature. Instead, they chose to remove the game from the App Store altogether.

It was clear yesterday morning that Epic knew what it was doing, and how Apple was going to respond. Shortly after Apple pulled Fornite from the App Store, Epic filed a lawsuit [1]. 

The lawsuit is savage, accusing Apple of things such as stifling innovation, controlling markets and blocking competition. It also reveals Epic's intentions of wanting to create their own alternative app store. This is from page 5 of the lawsuit:

 >> Epic — and Fortnite’s users — are directly harmed by Apple’s anti-competitive conduct. But for Apple’s illegal restraints, Epic would provide a competing app store on iOS devices, which would allow iOS users to download apps in an innovative, curated store and would provide users the choice to use Epic’s or another third-party’s in-app payment processing tool.

Epic is coming out with all guns blazing here. Not just content with wanting to have their own payment system, they are also saying that iOS should be as open to native third-party software as the Mac. 

Epic's also released a commercial [2] to make their argument. A parody of Apple's "1984" spot [3], it uses Fortnite's own unique brand. It closes with:

> Epic Games has defied the App Store Monopoly. In retaliation, Apple is blocking Fortnite from a billion devices. Join the fight to stop 2020 from becoming “1984”.

Whatever the result of the lawsuit is, it's already making waves all around the web, with people divided on what side to take. Ultimately, this might be a battle where public opinion can have a strong influence on the outcome.


[1] https://cdn2.unrealengine.com/apple-complaint-734589783.pdf
[2] https://www.youtube.com/watch?v=euiSHuaw6Q4
[3] https://www.youtube.com/watch?v=2zfqw8nhUwA




## Behavioral Biases

There's been a lot of newcomers to eToro since last year, so I thought it would be a good time to reshare some of my old articles with a few formatting improvements :-). This one is about behavioral biases in investing, or the attitudes we tend to adopt that affect our performance.

* Investors often have biases that influence their decisions, often to their detriment.
 * They invest when the market has gone up - and therefore when the chances of it going further up have decreased. And of course, they sell at the worst possible time too.
 * The impact of this bad practice is very significant, especially in volatile markets.
 * The solution is to plan your investment, and if possible to automate it.

The investor's psychology pushes him to make major mistakes. Indeed, the investor is his own enemy. Scientists have written about this, and you won't find a better book on the subject than "Thinking, Fast and Slow".

There are many behavioral biases among investors, which include, but are not limited to, the following:
 - Conservatism, that is, the tendency to dismiss the importance of new information. We often read about things that we already agree with. 
 - Drawn to simple, emotion-based arguments. A story that is well told and touches the heart is much more likely to be accepted than cold, academic reasoning.
 - Finding patterns in charts. Sometimes random is just random. 
 - Overconfidence. The vast majority of people feel better than the average, most of us feel better than our colleagues in our work, etc.
 - The attribution of our failures to bad luck and our successes to our skill.
 - The "I knew it". You always feel like you knew something would happen. The techno crisis? It was obvious that all these companies were overvalued...
 - Investors place more importance on a loss than on a gain of the same value. And when they are potentially in loss, they are willing to take absurd risks.
 - The "lottery ticket" effect. We prefer a low probability of making a huge gain rather than high probabilities of making an average gain... even knowing that the average gains are better in the long-term. 
 - Following the crowd. The tendency to do as others do. 
 - Anchoring, i. e. holding on to a reference number. You have bought your share at such a price, so you can't or won't sell below it...

I think these examples speak to you. Behavioral biases are certainly part of your daily routine. If you have already invested money or copied someone, I think you can see quite clearly how these behavioral biases influence your investment decisions and often do not take you in the right direction.

So how to fight your biases? The main thing is to be aware of them. If you are able to catch yourself, stop and asses the situation, you will have a better to chance of making a bias-less decision. 	

by @edoreld


## Getting Started

Conclusion:

Investing in ETFs is a safe long-term bet, but it can get a bit boring. A decent option.

Individual stock picking is more exciting but also more risky and requires a greater expense of time.

Copying a trader on the other hand has the potential for high returns, but requires that you pick a high quality one. 

My personal recommendation for long-term traders to copy are @MarianoPardo, @reinhardtcoetzee, @Couguar and @edoreld (myself). If all you want to do is invest your money somewhere and let it grow automagically, just put your money in one of those and don't look back.

## Add Funds

Hi everyone,

I will be adding funds to my account on September 10th.

This can affect your copy, so please read this post.

On September 10th, you will receive a notification telling you by which proportion I increase my funds. This is also the amount you should add to maintain the copy balanced. 

You have three options

1. Add more funds to your position 

This is the easiest method. If you increase your copy position by the same proportion as me, you will match my addition and maintain a synchronized copy.

If you do so, make sure not to copy open positions with the new funds.

I will not make any new investments until September 17th, so you will have 7 days to perform this action if you wish.

2. Stop copying and copy again 

You can stop copying and copy again between September 15-21 (make sure to copy open trades).

This way you can match the new proportion without adding funds, however, this also means that you will have to pay some spreads/fees.

3. Do nothing

You can keep your copy position as-is, but please know that some of the copied positions will change proportion.

Have a nice day,
Javier
 
