# Rational vs Irrational MInd

We are divided into two selves: rational and irrational.

The rational part is the youngest and weakest. It's also the one in charge of making high-level decisions.

The irrational part is the oldest and strongest. It concerns itself with making sure we are surviving.

The human struggle is trying to make rational decisions whilst overcoming the [[Impulses]] coming from our irrational self. 

