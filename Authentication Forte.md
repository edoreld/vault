

```mermaid
sequenceDiagram
  Note over Front,BFF: Demander le type d'authent nécessaire pour un virement
  Front->>+BFF: POST /user/fund-transfer/auth_type { creditor_iban }

  Note over BFF,Transfer Service: Demander au service des virements le type d'authent nécessaire
  BFF->>+Transfer Service: GET /auth_type { creditor_iban }
  
  Note right of Transfer Service: Déterminer le type d'authent nécessaire pour le virement
  Transfer Service->>Transfer Service: auth_type = déterminer_type_authent_nécessaire(creditor_iban)

  Transfer Service-->>-BFF: { auth_type: "mfa | password" }

  alt auth_type == "mfa"
    Note over BFF,AFC: Si AF nécessaire, récupérer l'éligibilité
    BFF->>+AFC: POST /authentications/eligibility
    AFC-->>-BFF: eligibility
  else
    BFF->>BFF: keypad = generate_keypad()
  end

  Note over Front,BFF: L'éligibilité existe que si une AF est nécessaire (sinon keypad existe)
  BFF-->>-Front: { auth_type, eligibility?, keypad? }

  alt auth_type == "mfa"
    Front->>+Front: Afficher parcours AF

    Note over Front,BFF: Initialiser AF
    Front->>+BFF: POST /user/fund-transfer/mfa/init { method: "SCAD|OTP",  ...allTransferData }

    Note over BFF,AFC: Initialiser AF avec les données du virement
    BFF->>+AFC: POST /authentications { method, ...allTransferData }

    AFC-->>-BFF: { uid }
    BFF-->>-Front: { mfa_uid }

    rect rgb(135, 206, 250)
      Note over Front,BFF: SEQUENCE AF par SCAD ou OTP
    end

    Note over Front,BFF: Finaliser virement avec type authent AF
    Front->>+BFF: POST /user/fund_transfer/mfa { mfa_uid }

    BFF->>+AFC: ???
    AFC-->>-BFF: { ...allTransferData }

    Note over BFF,Transfer Service: Déclencher virement avec AF
    BFF->>+Transfer Service: POST /funds_transfer/mfa { mfa_uid, ...allTransferData }

    Note over Transfer Service,AFC: Vérifier validité de l'authent avant de réaliser le virement
    Transfer Service->>+AFC: GET /authentications/${uid}/status
    AFC-->>-Transfer Service: { status, ...allTransferData }

    opt status is not "ok"
      rect rgb(240, 128, 128)
        Note over BFF,Transfer Service: Lever exception indiquant mauvais statut d'AF
        Transfer Service-->>BFF: Exception(BAD_MFA_STATUS)
      end
    end

    rect rgb(152, 251, 152)
      Note right of Transfer Service: SEQUENCE FAIRE LE VIREMENT
    end

    Note right of Transfer Service: SEQUENCE Invalider AF (mfa_uid)
  else
    Front->>+Front: Afficher saisie mot de passe

    Note over Front,BFF: Conserver positions du code secret + la graine (pour re-générer le clavier côté BFF)
    Front-->>-Front: keypad_positions + keypad_seed

    Note over Front,BFF: Finaliser virement avec type authent password
    Front->>+BFF: POST /user/fund_transfer/password { keypad_positions, keypad_seed, ...allTransferData }

    Note right of BFF: Re-générer le keypad à partir de la graine et reconstituer le code secret
    BFF->>BFF: password = getCode(keypad_positions + keypad_seed)

    Note over BFF,Transfer Service: Déclencher virement avec password
    BFF->>+Transfer Service: POST /funds_transfer/password { password, ...allTransferData }

    Note right of Transfer Service: Re-déterminer l'auth nécessaire pour le virement
    Transfer Service->>Transfer Service: auth_type = déterminer_type_authent_nécessaire()

    opt auth_type == "mfa"
      rect rgb(240, 128, 128)
        Note over BFF,Transfer Service: Lever exception indiquant AF requise
        Transfer Service-->>BFF: Exception(MFA_REQUIRED)
      end
    end

    Transfer Service->>+Authentication Service: POST /password
    Authentication Service-->>-Transfer Service: is_password_ok

    opt is_password_ok == false
      rect rgb(240, 128, 128)
        Note over BFF,Transfer Service: Lever exception indiquant mauvais mot de passe
        Transfer Service-->>BFF: Exception(BAD_PASSWORD)
      end
    end
    
    rect rgb(152, 251, 152)
      Note right of Transfer Service: SEQUENCE FAIRE LE VIREMENT
    end
  end

  Transfer Service-->>-BFF: Resultat
  BFF-->>-Front: Résultat
```