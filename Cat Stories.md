# Cat Stories

## Moon Guardian

### Story 1: Moon Guardian's mission

Long, Long time ago, there lived a cat whose name is lost in history. Other calls, however, called him Moon Guardian

**Moon Guardian’s task was to protect the moon,** no matter what threatened it

He trained long and hard, fighting plants along the world to increase his skills

He meditated to sharpen his mind and strengthen his willpower 	

 He explored far and wide, finding many rare species such as the pupper
 
 Naturally he also trained his sneaking and climbing skills!
 
 All to protect his precious moon
 
  From those daring enough to attack it!
  
  or the innocent-looking-but-actually-deadly-as-phuck Cuddlses
  
  And so Moon Guardian lived his days, always watching the moon and protecting it against its enemies
  
  ## Story 2: from cat to lion to kitten

One day, when Moon Guardian was minding his own business, he made a startling discovery

Half-buried beneath the grass, the found a heart shaped amulet!

Upon putting it on, **Moon Guardian grew and grew and grew, his body becoming many times its original size**

He was so startled by his sudden transformation that he even felt in the water! 

 He got out of the water and he became a bit depressed. This body was neat and all, but it wasn’t his!
 
 He wouldn’t take it anymore!
 
 He tried to take the amulet off, but to no avail 

So he went to see the wise owls of Huhulala, who told him that to take off the amulet he would need to visit the White Witch who lived in the forest

So he travelled far and wide, looking for this witch, until finally, after many months, reached his destination

And there she was, the White Witch!

"White Witch, White Witch", asked the distressed ex-kitty, "However shall I get back to my original cuter form"?

Well, first of all my name is not my White Witch, it’s Beatrix Balthazar, or B. B for short. Second, you have forgotten how to be a kitty. In order to become a kitty again, you must act like one! 

So for the next few days, the now-tiger acted fully like a kitten, snuggling with B. B., chilling doing cat things. 

And what do you know, in a few days he was a kitten again!

But wait a second. **A kitten? He used to be a full-grown cat!** How is he supposed to defend the moon now with this weak claws? Eventually he figured out a way to become an adult cat again, but that's a story for another place and for another time

## Story 3: from kitten to adult

Kitten Moon Guardian was distraught

He looked at the moon and wondered, how can I protect you if I am this smol ?

He thought long and hard for many days about becoming adult cat again

And eventually he had an idea! Planning to put it to use in the morning, he contentedly went to bed 

His idea was such. If to become a kitten, he had to act like a kitten. Then to become an adult cat, he needed to act like an adult cat. But what the difference between adult cats and kittens? 

Why, experience of course! Adult kittens had lots of experiences in lots of different activities! He would need to experience lots of things in order to grow up

And so Moon Guardian set out into the world, his aim clear. 

He experienced many things in his travels.

He experienced the pain of isolation 

He experienced the sleepiness of waking up early 

He experienced the bop of a friendly butterfly 

And the joy of true friendship 

Little by little, without even noticing, he grew and grew and grew, until one day, looking in the mirror, he saw he had become an adult again!

He happily went to the the moon about his growth, but the moon was nowhere to be found! In all his growing, he hadn’t even looked at the moon once, and now the moon was gone!

“Oh fate, thou art cruel” - thought the cat - “at the time of my greatest triumph thou taketh the object of my affection”

It was clear, Moon guardian new mission would be to find the missing moon!

## Story 4: retrieving the moon

Where could the moon be ? - wondered Moon Guardian

He looked for her here

He looked for her there

He looked everywhere 

He traveled, asking strangers if they had seen her, but to no avail

He had almost given up. Distraught, he started to give in to his inner darkness… 

But suddenly, a great light shone on his face

The moon was back! 

“Oh Moon” - asked Moon Guardian - “where were you? I looked for you everywhere”

The moon, after clearing its throat said: “Oh, didn’t I tell you, I was just doing some shopping”

At that point Moon Guardian was pretty mad at the moon for not telling him she was gone, so he meowed and meowed and meowed all night long. 

And that’s the story of how the moon was “recovered”




