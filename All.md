# All

## Taxes in France (impôts)

La déclaration n°2042 est la déclaration des revenus. Elle permet au contribuable de déclarer tous les revenus perçus par l'ensemble des membres qui composent son foyer fiscal.

La déclaration n°2042-C est la déclaration des revenus complémentaire. Elle permet au contribuable de déclarer les revenus et les charges qui ne figurent pas sur la déclaration n°2042.

La déclaration n° 2044 est la déclaration des revenus fonciers. Doivent y être mentionnés, tous les revenus locatifs immobiliers autres que ceux concernés par la déclaration spéciale n°2044-SPE.

La déclaration n°2044-SPE est une déclaration des revenus fonciers spéciale qui permet de déclarer les revenus locatifs provenant d'immeubles bénéficiant de régimes fiscaux particuliers, tels que les monuments historiques, les immeubles situés dans un secteur particulier, les biens éligibles aux dispositifs De ROBIEN, SCELLIER, etc...

La déclaration n°2074 permet au contribuable de déclarer les plus ou moins-values sur cessions de valeurs mobilières, droits sociaux, titres assimilés et les clôtures de PEA, le MATIF, les marchés d'options négociables et les bons d'option, les cessions de parts de FCIMT.

La déclaration n°2047 permet de déclarer les revenus étrangers encaissés par un contribuable français. Si un membre du foyer fiscal a encaissé des revenus hors de France métropolitaine et des départements d'outre mer, il est nécessaire de remplir cette déclaration.

La déclaration n°3916 permet au contribuable de déclarer les comptes bancaires détenus à l'étranger. Les particuliers mais aussi les sociétés ou associations qui n'ont pas la forme commerciale ont l'obligation de déclarer les comptes dont ils sont titulaires à l'étranger en même temps que la déclaration de revenus ou de résultats.


## Web Development (front)

### CSS

Element in a page are positioned according to a stack. **Elements with greater stack order are in front of elements with lower stack order**.

The `z-index` property places elements in the stack. 

