# Flow
#productivity

Synonyms: [[Flow | Transient Hypofrontality]]

 - **A state of mind in which we feel full enjoyment through being fully immersed and involved in an activity**
 - Involves a temporary lowering of activity in the analytical part of our brain:  the [[Prefrontal Cortex]].
 - Flow is enabled by Instant Feedback, Clear Goals and a suitable Challenge-Skill Ratio [[i+1]].

## i.e
 - Reading an engaging book
 - Runner's high
 -
 
 