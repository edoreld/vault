# Getting a mortgage

Related to: [[Buying a house]]

## What

Getting a mortgage would mean that I would pay the cost of the house over 20 years

## Why

It would allow me to put dad's money to use in investment opportunies.

The basic idea is that I would pay 1-2% interest on a loan to buy an appartment while at the same time I would invest the money in the stock market which over 20 years would potentially return between 7% and 10% (5% and 8% after taxes).

## How

Send dossier to different banks and get different propositions for a loan. 

## Blocks
 - Calling the new accounts-service url leads to a certificate error. In the past I've solved these errors by building a new certificate with the url of the service to call, but in the new projects I am missing the relevant scripts to do so.
 - I've tried calling the `build-cert.sh` script from the `common-ci-cd` repo but it doesn't download any certificate.  