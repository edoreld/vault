# Class! JsonPrinter

package com.cl.sit.transfer.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonPrinter {

    private static final ObjectMapper mapper= new ObjectMapper();

    public static void printJson(Object o) {
        try {
            System.out.println(mapper.writeValueAsString(o));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }
}
