# Ways to manage ego depletion when on the computer
#willpower #self-control #productivity #procrastination 

Browing the Internet invariably leads to impulses. This is normal. Impulses must be managed somehow. 

Dealing with impulses is akin to risk management. Impulses must be Accepted, Avoided, Mitigated or Dodged. 

Accepting an impulse is giving up any semblance of self-control. It takes 0 willpower but it also leads to the negative consequence that we are trying to avoid. 

Avoiding an impulse would be to remove the sources of impulse from my life. 



