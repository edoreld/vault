# C! Building a second brain with Tiago Forge
#course
- Author: Tiago Forte
- Related to: [[Productivity]], [[Organization]]

## Notes 

### Unit 1 Introduction
 - Preface: [[V! Building a second brain with Tiago Forte | Building a second brain with Tiago Forte]]
 - The objective of the course is to **learn a system based on digital notes that is useful in collecting, organizing and retrieving ideas and maximizes creativity**

 - Tiago distinguishes **3 progressive levels of proficiency in note-taking**: storing information, intermediate thinking and creative breakthroughs:
   - Storing Information is simply putting stuff into our note taking system
   - **Intermediate thinking is when the note system is the basis for the creation of art**. It enables to break down big problems into small, doable ones.  
	   > “The object isn't to make art, it's to be in that wonderful state which makes art inevitable.” - Robert Henri
   - **Creative Breakthroughs** is when the note taking system leads to new ideas, thoughts and connections.
   
<mark>We develop a system based on digital, interconnecting notes that supports the creation of art and enables creative walkthroughs</mark>

### Unit 2: Organizing for Creativity

- Are organizing and creative activity opposed concepts?
- A note taking system that promotes creativity must have these four characteristics: 
	+ Promote links and associations between the different notes.
		> ".. increased sensitivity to unusual associations is another important contributor to creativity." - Harvard Business Review
		- **Keeping all your data in one place promotes links, associations and cross-pollination of the information**
	 + Create visual artifacts
		 - He gives the example of scientists who deal with abstract thoughts and yet build physical models. **We build models because we think better visually than with abstract thoughts.**
		 - He shows his Evernote notes where most notes have an image associated with them. 
	  + In summary. **incubate ideas over time by working on multiple projects at the same time and reusing and recycling ideas**
		 - Heavy lifting vs Slow Burn 
			 	- Heavy Lifting: tackling a project as something to do in one big session. He accepts that sometimes it might be necessary, but he says it's not sustainable.
				- Slow burn: doing multiple projects gradually, using small, disconnected pockets of time to add value to each
			> "You have to keep a dozen of your favorite problems constantly present in your mind, although by and large they will lay in a dormant state. Every time you hear or read a new trick or a new result, test it against each of your twelve problems to see whether it helps. Every once in a while there will be a hit, and people will say, 'How did he do it? He must be a genius!" - Richard Feynman
		- Reusing and Recycling
			- Using the notes that we've taken for a certain project and use that knowledge for future projects. 
	+ Provide the raw material for unique interpretations and perspectives
		- **Information's value is in the interpretation we give it and the persuasion with wich we present it to others.**
		- **The variety of ways in which we can display information (images, mindmaps, videos, etc) can lead to unique interpretations and arguments for persuasion.**

### Unit 3: Maximizing Return-on-Attention
- **Return-on-attention** (ROA) is **the value we get out of paying attention**. Our note-taking system should maximize this value and deliver a positive ROA. 
 - He talks about [[Flow]]. 
- Any work has three different **preparation phases we need to go through before starting that work**;
	 - Environment setup: opening applications and websites, setting up the desk, etc.
	 - Mental Setup: what am I working on? 
	 - Emotional Setup: fighting the [[Resistance]]. 
 - **These phases consume our attention**, yet we need to go through them in order to reach the desired flow state.
 - When we deliver a functionality we are developing, all the work involved in getting to that functionality is lost, because we don't have a system of recycling and reusing the information. This can make us have to start future projects from zero instead of being able to rely on work we've done before. 
 - A possible remedy to this is <mark>**[[Placeholding]]: iteratively building value through smaller deliveries</mark> - "intermediate packets" or "modules" - which are packaged in a certain way that makes it possible to exploit them for future projects.**
 - Placeholding refers to placing "placeholders" along places in your projects, so you can come back to them later. 
 - **Examples of placeholding** would be the **notes, designs, illustrations**, charts, graphics, mindmaps, etc, that we iteratively build as we work on a project. 
 - There are at least three benefits for working this way:
	 - Harder to interrupt: no need to keep the whole project in your head, so interruptions' impact is lessened
	 - Easier to get feedback: it's easier for others to give feedback on small changes
	 - Able to work always on a task, no matter how little or how much time you have for that working session.
	 - Insights from previous projects can be used for new ones. Each project becomes a series of "lego pieces" which we can assemble in new ways to create different value.   [[Network Effect]] makes it so that new knowledge built on top of old one adds exponential value. 

- So what's the **relation between [[Flow]] and building projects as small packets of value**? If we take a look at some of the requirements for flow, we have:
	-  Instant Feedback, 
	-  Clear Goals
	-  Suitable Challenge-Skill Ratio [[i+1]]
 - And those are exactly the things enabled by working in small increments of value, such as with a note system. 
- The session finishes with a hint of next unit, by showing how the process of encoding and retrieving information from our intermediate packages of value must be intertwined with our work. 

### Unit 4: Progressive Summarization

 - notes > notebooks (think Evernote) > tags
 - What we care about is the notes themselves and their contents, now how they are organized. 
 - **Notes are all about Discoverability vs Understanding**:
	 - **Discoverability: compressing note** into metadata that allows us to get the meaning of the note quickly
	 - **Understanding: filling the note with plenty of examples and explanations** that make it easier to understand it.
 - <mark>Discoverability => Compression => Encoding</mark>
 - <mark>Understanding => Explanations => Decoding</mark>
 
 - **<mark>Progressive summarization is the process of iteratively summarizing (compressing) your note</mark>, over and over, until you get to a point where you can understand a note without needing to read the whole thing first**, thereby preventing loss of value due to needing to read a note before deciding if it's relevant. 
 - Tiago implements **progressive summarization though progressive layering**:
	 1. No filtering
	 2. Bold interesting parts
	 3. Highlight interesting-er parts
	 4. Mini-summary
	 5. Remix: creating your own content based on your interpreation of your notes (sort of like when creating illustrations for concepts in a bullet journal)
 - Primes the note for future you's understanding. 
 - One way to consume notes is to ask the question: **how does this note help with my current problem?**
 - One way to design notes is to think: how do I make as easy as possible to process these notes for future me? How do I make it as simple as possible to understand them for him, specially when he's tired, unmotivated, etc. 

### Unit 5: The Design of Discoverability 

The lecture is about **enhancing discoverability through design**. It's based on the book [[B! The Design of Everyday Things]] and the principles for discoverability contained therein:
 - Affordance: the link between the properties of an object and a person's capabilities that determines how the object should be used.
 - Signifiers: cues of what an object's behavior or meaning is.
	 - i.e: a sign on a door that says pull, a button with an X to close an app
 - Constraints: limitation on what the user can do. They funnel a person toward certain actions and ways of interacting with an object.
	 - i.e: Shape of a bin's mouth
	 - i.e: filtering what you write into a note, what you copy and paste, how you format things, etc.
 - Mappings: the relationship between a control with the actions that can be taken with it and the result of those actions
 - Feedback: communicating the results of an action or the current state of the system.
	 -  Weather monitor showing a display
	 -  Ethernet port's green light
 - **Good design removes barriers to [[Flow]].** 

### Unit 6: Workflow & Retrieval

 - Tiago defines **a strategy as an approach to tackling a problem that consists of identifying the problem, the desired solution, and an action plan**. He calls these three steps diagnosis, guiding policy and action plan. 
 - The key is to identify these three stages *before* tackling the problem.
 - Then he goes on to describe **the process of creating something as a consisting of two phases: elaboration and convergence**:
	 - Elaboration: brainstorming, casting a wide net
	 - Convergence: navigating toward a certain projected solution/direction
 - In the elaboration phase, we want to **"paint in broad strokes"** rather than focusing on minute detail that can bog us down. It's like when we are writing a draft: corrections, grammar & spelling checks, etc, slow the creative process. 
 > Early in the problem solving process, **accurate but imprecise methods**, rather than very exact methods, will allow **consideration of all reasonable approaches** and minimize the tracking of needlessly detailed data
 > -- John Kuprenas
